var gulp = require('gulp');
var browserSync = require('browser-sync').create();

gulp.task('browser-sync', function(){
		browserSync.init({
			proxy: "http://localhost/domo/wordpress"
		});
    	gulp.watch(['css/**.css', './*.php', './**/*.php']).on('change', browserSync.reload);
	});