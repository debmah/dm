<?php get_header();
/* Template Name: Home*/
?>

<div class="content-home pt-1em">
  <div class="container">
    <div class="featured_products_show">
      <div class="row clearfix">
        <?php
        $sections = get_field("sections");
        if (count($sections) == 1){
          $section_1 = $sections[0];
          ?>
          <div class='col-sm-12'>
            <a href='<?php Print $section_1['link']; ?>' class="block-element">
              <div class="featured_item full_width_img first_featured" style="background-image: url('<?php Print $section_1['image']['sizes']['large']; ?>');"></div>
              <div class="featured-details darkblue" style='background:<?php Print $section_1['colour'];?>'>
                <p><?php Print $section_1['text_line_one']; ?></p>
                <p><?php Print $section_1['text_line_two']; ?></p>
                <div class="triangle-bottomright"></div>
              </div>
            </a>
          </div>
          <?php
        }

        if (count($sections) == 2){
          $section_1 = $sections[0];
          $section_2 = $sections[1];
          ?>
          <div class='col-sm-6'>
            <a href="<?php Print $section_1['link']; ?>" class="block-element">
              <div class="featured_item_long half_width_full" style="background-image: url('<?php Print $section_1['image']['sizes']['large']; ?>');"></div>
              <div class="featured-details brown" style='background:<?php Print $section_1['colour'];?>'>
                <p><?php Print $section_1['text_line_one']; ?></p>
                <p><?php Print $section_1['text_line_two']; ?></p>
                <div class="triangle-bottomright"></div>
              </div>
            </a>
          </div>
          <div class='col-sm-6'>
            <a href="<?php Print $section_2['link']; ?>" class="block-element">
              <div class="featured_item_long half_width_full" style="background-image: url('<?php Print $section_2['image']['sizes']['large']; ?>');"></div>
              <div class="featured-details brown" style='background:<?php Print $section_2['colour'];?>'>
                <p><?php Print $section_2['text_line_one']; ?></p>
                <p><?php Print $section_2['text_line_two']; ?></p>
                <div class="triangle-bottomright"></div>
              </div>
            </a>
          </div>
          <?php
        }

        if (count($sections) == 3){
          $section_1 = $sections[0];
          $section_2 = $sections[1];
          $section_3 = $sections[2];
          ?>
          <div class="col-sm-6"> <a href="<?php Print $section_1['link']; ?>" class="block-element">

            <div class="featured_item half_width_half first_featured" style="background-image: url('<?php Print $section_1['image']['sizes']['large']; ?>');"></div>
            <div class="featured-details darkblue" style='background:<?php Print $section_1['colour'];?>'>
              <p><?php Print $section_1['text_line_one']; ?></p>
              <p><?php Print $section_1['text_line_two']; ?></p>
              <div class="triangle-bottomright"></div>
            </div>
          </a> <a href="<?php Print $section_2['link']; ?>" class="block-element">
            <div class="featured_item half_width_half" style="background-image: url('<?php Print $section_2['image']['sizes']['large']; ?>');"></div>
            <!-- <img src="img/colorbox2.jpg" alt="featured item"> -->
            <div class="featured-details darkbrown" style='background:<?php Print $section_2['colour'];?>'>
              <p><?php Print $section_2['text_line_one']; ?></p>
              <p><?php Print $section_2['text_line_two']; ?></p>
              <div class="triangle-bottomright"></div>
            </div>
          </a> </div>
          <div class="col-sm-6"> <a href="<?php Print $section_3['link']; ?>" class="block-element">
            <div class="featured_item_long half_width_full" style="background-image: url('<?php Print $section_3['image']['sizes']['large']; ?>');"></div>
            <div class="featured-details brown" style='background:<?php Print $section_3['colour'];?>'>
              <p><?php Print $section_3['text_line_one']; ?></p>
              <p><?php Print $section_3['text_line_two']; ?></p>
              <div class="triangle-bottomright"></div>
            </div>
          </a> </div>
          <?php
        }
        ?>
      </div>
    </div>
    <!-- featured products show -->

    <div class="whitecontentbox ptb-4em">
      <h2><?php echo get_field('cta_title'); ?> <img src="<?php bloginfo('template_url'); ?>/img/domo.svg" alt="domo"></h2>
      <p><?php echo get_field('cta_subtitle'); ?></p>
      <a href="<?php echo get_field('cta_button_link'); ?>"><?php echo get_field('cta_button_label'); ?> <i class="fa fa-angle-right ml-8"></i></a>
      <div class="triangle-bottomright"></div>
    </div>
    <?php
    //Get the brand profiles
    $brand_profile_ids = array();
    $brand_profile_1 = get_field("brand_profile_1");
    if ($brand_profile_1 == NULL){
      $brand_profile_1 = get_random_brand_profile($brand_profile_ids);
    }
    array_push($brand_profile_ids, $brand_profile_1->ID);

    $brand_profile_2 = get_field("brand_profile_2");
    if ($brand_profile_2 == NULL){
      $brand_profile_2 = get_random_brand_profile($brand_profile_ids);
    }
    array_push($brand_profile_ids, $brand_profile_2->ID);

    $brand_profile_3 = get_field("brand_profile_3");
    if ($brand_profile_3 == NULL){
      $brand_profile_3 = get_random_brand_profile($brand_profile_ids);
    }
    ?>
    <div class="brand-profile"> <a href="<?php Print get_permalink($brand_profile_1->ID);?>" class="block-element">
      <div class="brandprofile_full full_width_img brand_profile_box" style="background-image: url('<?php Print get_field("lifestyle_image", $brand_profile_1->ID)['sizes']['large']; ?>');">
      </div>
      <div class="featured-details">
        <p>brand profile</p>
        <h3><?php Print $brand_profile_1->post_title; ?></h3>
        <div class="triangle-bottomright"></div>
      </div>
    </a>
    <br />
    <div class="row clearfix">
      <div class="col-sm-6 mmb-2em"> <a href="<?php Print get_permalink($brand_profile_2->ID);?>" class="block-element">
        <div class="featured_item half_width_half" style="background-image: url('<?php Print get_field("lifestyle_image", $brand_profile_2->ID)['sizes']['large']; ?>');">
        </div>
        <div class="featured-details">
          <p>brand profile</p>
          <h3><?php Print $brand_profile_2->post_title; ?></h3>
          <div class="triangle-bottomright"></div>
        </div>
      </a> </div>
      <div class="col-sm-6"> <a href="<?php Print get_permalink($brand_profile_3->ID);?>" class="block-element">
        <div class="featured_item half_width_half" style="background-image: url('<?php Print get_field("lifestyle_image", $brand_profile_3->ID)['sizes']['large']; ?>');">
        </div>
        <!-- <img src="img/colorbox2.jpg" alt="featured item"> -->
        <div class="featured-details">
          <p>brand profile</p>
          <h3><?php Print $brand_profile_3->post_title; ?></h3>
          <div class="triangle-bottomright"></div>
        </div>
      </a> </div>
    </div>
  </div>
  <!-- brand profile -->

  <div class="domo-newsletter">
    <div class="row clearfix">
      <div class="col-sm-7 col-md-8 pr-0">
        <div class="newsletter_brandimage" style="background-image:url('<?php echo get_field("newsletter_image"); ?>')">
          <div class="newsletter-infobox">
            <h2> DOMO <span>NEWS & REWARDS</span></h2>
            <p><?php echo get_field("newsletter_subtitle"); ?></p>
          </div>
        </div>
      </div>
      <div class="col-sm-5 col-md-4 pl-0">
        <div class="newsletter_box homepage">
          <div class="title_box">
            BECOME A DOMO MEMBER <br>& BE REWARDED
          </div>

          <div class="newsletter_tab">
          	  <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#retail" aria-controls="retail" role="tab" data-toggle="tab">Retail<span>Member Benefits</span><small class="minitriangle triangle-bottomright"></small></a></li>
                <li role="presentation"><a href="#trade" aria-controls="trade" role="tab" data-toggle="tab">Contract<span>Member Benefits</span><small class="minitriangle triangle-bottomright"></small></a></li>
              </ul>
              <div class="tab-content newsletter_tab_content">
                <div role="tabpanel" class="tab-pane active" id="retail">
                	<ul>
                    	<!--<li class="star">10% OFF FOR REGISTERED MEMBERS<span>ON CLICK & COLLECT PRODUCTS ONLY</span></li>-->
                        <li class="tag">EXCLUSIVE NOTIFICATION<span>OF SALES & PROMOTIONS</li>
                          <li class="ticket">INVITATIONS TO DOMO EVENTS & PRODUCT LAUNCHES</li>
                        <li class="comment">DOMO NEWSLETTER<span>EMAILED DIRECT TO YOU</li>
                    </ul>
                </div>
                <div role="tabpanel" class="tab-pane" id="trade">
                	<ul>
                    	<li class="star">EXCLUSIVE LOYALTY<span> & DISCOUNT PROGRAM</span></li>
                        <li class="tag">INVITATIONS TO BRAND<span> & PRODUCT LAUNCHES</li>
                        <li class="comment">DEDICATED CONSULTANT<span>TO SUPPORT YOUR BUSINESS</li>
                    </ul>
                </div>
              </div>
          </div>

          <div class="signup_button">
          	<a href="<?php echo get_permalink(210); ?>">SIGN UP NOW</a>
          </div>


          <?php /*?><div class='newsletter_box_content'>
            <div class='row'>
              <div class='col-sm-2'>
                <i class="fa fa-star" aria-hidden="true"></i>
              </div>
              <div class='col-sm-10 text-left'>
                <b>10% OFF FOR MEMBERS</b>
                <br>
                FOR ONLINE PRODUCTS ONLY
              </div>
            </div>
            <br>
            <div class='row'>
              <div class='col-sm-2'>
                <i class="fa fa-tag" aria-hidden="true"></i>
              </div>
              <div class='col-sm-10 text-left'>
                <b>DISCOUNT OFFERS & EXCLUSIVE MEMBER SALES</b>
              </div>
            </div>
            <br>
            <div class='row'>
              <div class='col-sm-2'>
                <i class="fa fa-comment" aria-hidden="true"></i>
              </div>
              <div class='col-sm-10 text-left'>
                <b>DOMO RETAIL NEWSLETTER</b>
              </div>
            </div>
            <br>
            <div class='row'>
              <div class='col-sm-2'>
                <i class="fa fa-file-text" aria-hidden="true"></i>
              </div>
              <div class='col-sm-10 text-left'>
                <b>EXCLUSIVE TRADE NEWSLETTER</b>
                <br>
                FOR THOSE IN THE INDUSTRY
              </div>
            </div>
            <br><br>
            <div class='row'>
              <div class='col-sm-12 text-center'>
                <a class='btn' href='<?php Print get_site_url(); ?>/sign-up'>SIGN UP</a>
              </div>
            </div>
          </div><?php */?>
          </div>
        </div>
      </div>
    </div>
    <!-- domo newsletter -->

    <div class="domo-news pt-1em pb-3em">
      <div class="centered_header pb-2em">
        <h2>Domo <span>News</span></h2>
      </div>
      <div class="news-cards">
        <div class="row clearfix">
          <?php
          $posts = get_posts(array('orderby' => 'date', 'order' => 'desc', 'numberposts' => 3, 'post_type' => 'post'));
          foreach($posts as $post){
            setup_postdata($post);
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
            ?>
            <div class="col-sm-4 mmb-2em news_list_block">
              <article>
                <figure><a href="<?php the_permalink(); ?>"><div class='news_image' style='background-image:url("<?php Print $image[0]; ?>");'></div></a></figure>
                <div class="news_content">
                  <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                  <?php the_excerpt(); ?>
                  <a href="<?php the_permalink(); ?>" class="view_more_btn">View <i class="fa fa-angle-right ml-8"></i></a> </div>
                </article>
              </div>
              <?php
            }
            ?>
          </div>
          <div class="view-all-section pt-2em"><a href="<?php Print get_site_url(); ?>/news">View all news <i class="fa fa-angle-right ml-8"></i></a></div>
        </div>
      </div>
      <!-- domo news -->
    </div>
    <!-- container -->
    <div class="domo-shop-online" style="display:none">
      <div class="pt-4em pb-3em">
        <div class="container">
          <div class="centered_header pb-1em">
            <h2>Shop <span>Online</span></h2>
          </div>
          <h5 class="text-center text-uppercase mini_info">For accessories and decorations</h5>
          <?php
          /* Retrieve the for online products */
          $args = array(
            'post_type' => 'product',
            'orderby'   => 'rand',
            'posts_per_page' => '12',
            'meta_key'		=> 'available_for_online_purchase',
	          'meta_value'	=> true
          );
          $query = new WP_Query( $args );
          ?>
          <div class="online-product-slider pt-3em">
            <?php if ( $query->have_posts() ) : ?>
              <?php while ( $query->have_posts() ) : $query->the_post(); ?>
                <?php
                $brand_terms = get_the_terms(get_the_ID(), "brand");
                $brands = array();
                foreach($brand_terms as $term){
                  array_push($brands, $term->name);
                }
                ?>
                <div class="itemslider">
                  <a href="<?php Print get_permalink(); ?>">
                    <figure><?php echo get_the_post_thumbnail(get_the_ID(), 'shop_catalog'); ?></figure>
                    <h4><?php Print strtoupper(get_the_title()); ?></h4>
                    <h5><?php Print strtoupper($brands[0]); ?></h5>
                  </a>
                </div>
              <?php endwhile; ?>
              <?php wp_reset_postdata(); ?>
              <?php else : ?>
            <?php endif; ?>
          </div>
          <div class="view-all-section pt-3em"><a href="<?php Print get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>">View shop products <i class="fa fa-angle-right ml-8"></i></a></div>
        </div>
      </div>
    </div>
    <!-- shop online -->
  </div>
  <!-- content home-->
  <?php get_footer(); ?>
