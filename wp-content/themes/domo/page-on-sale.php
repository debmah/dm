<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'alt' ); ?>

<div class="content ptb-2em">
	<div class="container">
    <div class='row'>
      <div class='col-sm-12'>
        <div class='large-page-lifestyle-image' style='background-image: url("<?php Print get_field("image")['sizes']['large']; ?>")'>
          <div class='latest-arrivals-banner-text'>
						<h2 class='bold'><?php Print get_field("header_line_1"); ?></h2>
            <h2 class='light'><?php Print get_field("header_line_2"); ?></h2>
          </div>
        </div>
      </div>
    </div>
		<?php /* original by Matt
		<div class='row product-filter-row'>

			<script>
			function createProductFilters(){
        //No filters need to be created
			}

			function createProducts(){
				var template = "";
				template += "<div class='col-sm-6 col-md-4 col-lg-3 product-filter-item' data-brand='{{brand}}' data-style='{{style}}' data-product-cat='{{product_cat}}' data-upholstery='{{upholstery}}' data-domo-product-type='{{domo_product_type}}'>";
				template += "	<a href='{{link}}'>";
				template += "  <div class='domo-product'>";
				template += "   <div class='image'>{{image}}</div>";
				template += "   <span class='product-title'>{{name}}</span>";
				template += "   <span class='product-brand'>{{brand_name}}</span>";
				template += "   <div class='product-tags'>{{sale}}{{promotion}}{{clearance}}</div>";
				template += "  </div>";
				template += " </a>";
				template += "</div>";
				ProductFilter.setProductTemplate(template);

				<?php
				$cached_products = get_transient("sale_products_cache");

				if ($cached_products){
					?>
					ProductFilter.setProducts("<?php Print base64_encode(gzcompress($cached_products)); ?>", function(){
						ProductFilter.search("");
						ProductFilter.search("<?php Print $_GET['search']; ?>");
					});
					<?php
				} else {
					?>
					ProductFilter.setProducts([]);
					<?php
				}
				?>
			}

			</script>

		</div> */ ?>

		<br />
		<div class='row product-filter-row'>
			<?php
			$cached_products = get_transient("sale_products_cache");
			$category = get_queried_object();
			$category = $category->slug;
			if ($cached_products != "[]" && $cached_products != null){
			?>

			<!--Category filter -->
			<div class='product-filter dropdown' id='product-category-filter'>
				<span class='product-filter-label desktop'></span>
				<button class="btn btn-default product-filter-btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					<span class='product-filter-label'></span>
					<span class='right-chevron'><i class='fa fa-chevron-right'></i></span>
					<span class='product-filter-current-value'></span>
					<span class="caret"></span>
					<span class="mobile-caret"><i class='fa fa-chevron-down'></i></span>
				</button>
				<ul class="dropdown-menu product-filter-dropdown"></ul>
			</div>

			<!--Product filter -->
			<div class='product-filter dropdown' id='product-product-filter'>
				<span class='product-filter-label desktop'></span>
				<button class="btn btn-default product-filter-btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					<span class='product-filter-label'></span>
					<span class='right-chevron'><i class='fa fa-chevron-right'></i></span>
					<span class='product-filter-current-value'></span>
					<span class="caret"></span>
					<span class="mobile-caret"><i class='fa fa-chevron-down'></i></span>
				</button>
				<ul class="dropdown-menu product-filter-dropdown"></ul>
			</div>

			<!--Brand filter -->
			<div class='product-filter dropdown' id='product-brand-filter'>
				<span class='product-filter-label desktop'></span>
				<button class="btn btn-default product-filter-btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					<span class='product-filter-label'></span>
					<span class='right-chevron'><i class='fa fa-chevron-right'></i></span>
					<span class='product-filter-current-value'></span>
					<span class="caret"></span>
					<span class="mobile-caret"><i class='fa fa-chevron-down'></i></span>
				</button>
				<ul class="dropdown-menu product-filter-dropdown"></ul>
			</div>

			<!--Style filter -->
			<div class='product-filter dropdown' id='product-style-filter'>
				<span class='product-filter-label desktop'></span>
				<button class="btn btn-default product-filter-btn" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
					<span class='product-filter-label'></span>
					<span class='right-chevron'><i class='fa fa-chevron-right'></i></span>
					<span class='product-filter-current-value'></span>
					<span class="caret"></span>
					<span class="mobile-caret"><i class='fa fa-chevron-down'></i></span>
				</button>
				<ul class="dropdown-menu product-filter-dropdown"></ul>
			</div>


			<script>
			<?php
			//Get all the style terms
			$style_terms = get_terms( 'style', array('hide_empty' => true) );
			$styles = array();
			array_push($styles, array("value" => "*", "name" => "All Styles"));
			foreach($style_terms as $term){
				array_push($styles, array("value" => $term->slug, "name" => $term->name));
			}

			//Get all the category terms
			$category_terms = get_terms( 'product_cat', array('hide_empty' => true) );
			$categories = array();
			array_push($categories, array("value" => "*", "name" => "All Categories"));
			foreach($category_terms as $term){
				array_push($categories, array("value" => $term->slug, "name" => $term->name));
			}

			//Get all the product terms
			$product_terms = get_terms( 'domo_product_type', array('hide_empty' => true) );
			$product = array();
			array_push($product, array("value" => "*", "name" => "All Products"));
			foreach($product_terms as $term){
				array_push($product, array("value" => $term->slug, "name" => $term->name));
			}

			//Get all the brand terms
			$brand_terms = get_terms( 'brand', array('hide_empty' => true, 'parent' => 0) );

			$brands = array();
			array_push($brands, array("value" => "*", "name" => "All Brands"));
			foreach($brand_terms as $term){
				array_push($brands, array("value" => $term->slug, "name" => $term->name));
			}

			//Get all the upholstery terms
			$upholstery_terms = get_terms( 'upholstery', array('hide_empty' => true) );
			$upholstery = array();
			array_push($upholstery, array("value" => "*", "name" => "All"));
			foreach($upholstery_terms as $term){
				array_push($upholstery, array("value" => $term->slug, "name" => $term->name));
			}
			?>

			function createProductFilters(){
				ProductFilter.setPageTitleEnabled(false);
				ProductFilter.create("#product-style-filter", {
					id: "style", label: "STYLE", defaultValue: "*",
					values: <?php Print(json_encode($styles)); ?>,
					queryKey: 'filter_style'
				});

				ProductFilter.create("#product-category-filter", {
					id: "product_cat", label: "CATEGORY", defaultValue: "*",
					values: <?php Print(json_encode($categories)); ?>,
					queryKey: 'filter_product_cat',
					<?php if ($category != null){ Print "selected: '" . $category . "'"; } ?>
				});

				ProductFilter.create("#product-brand-filter", {
					id: "brand", label: "BRAND", defaultValue: "*",
					values: <?php Print(json_encode($brands)); ?>,
					queryKey: 'filter_brand'
				});

				ProductFilter.create("#product-product-filter", {
					id: "domo_product_type", label: "PRODUCT", defaultValue: "*",
					values: <?php Print(json_encode($product)); ?>,
					queryKey: 'filter_product_type'
				});

				ProductFilter.create("#product-upholstery-filter", {
					id: "upholstery", label: "UPHOLSTERY", defaultValue: "*",
					values: <?php Print(json_encode($upholstery)); ?>,
					queryKey: 'filter_upholstery'
				});
			}
			</script>
			<?php } ?>
			<script>

			function createProducts(){
				var template = "";
				template += "<div class='col-sm-6 col-md-4 col-lg-3 product-filter-item' data-brand='{{brand}}' data-style='{{style}}' data-product-cat='{{product_cat}}' data-upholstery='{{upholstery}}' data-domo-product-type='{{domo_product_type}}'>";
				template += "	<a href='{{link}}'>";
				template += "  <div class='domo-product'>";
				template += "   <div class='image'>{{image}}</div>";
				template += "   <span class='product-title'>{{name}}</span>";
				template += "   <span class='product-brand'>{{brand_name}}</span>";
				template += "   <div class='product-tags'>{{sale}}{{promotion}}{{clearance}}</div>";
				template += "  </div>";
				template += " </a>";
				template += "</div>";
				ProductFilter.setProductTemplate(template);

				<?php

				if ($cached_products){
					?>
					ProductFilter.setProducts("<?php Print base64_encode(gzcompress($cached_products)); ?>", function(){
						ProductFilter.search("");
						ProductFilter.search("<?php Print $_GET['search']; ?>");
					});
					<?php
				} else {
					?>
					ProductFilter.setProducts([]);
					<?php
				}
				?>
			}

			</script>

		</div>

		<?php if ($cached_products == "[]" || $cached_products == null){
			?>
			<div class='no-products-found'>
				<b>There are currently no items on sale</b>
				<p>Click below to view our full furniture range</p>
				<a href='<?php Print get_site_url(); ?>/shop' class='button'>DOMO FURNITURE RANGE</a>
			</div>
			<?php
		} ?>
		<div class='row product-holder'>

		</div>
	</div>
</div>
<?php get_footer( 'alt' ); ?>
