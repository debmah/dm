<?php
get_header(); ?>
<div class="content ptb-2em">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class='brand-profile-lifestyle-img' style='background-image:url("<?php Print get_field("lifestyle_image")['sizes']['large']; ?>")'>
          <?php /*?><div class='brand-profile-bar'>
            BRAND PROFILE // <b><?php Print strtoupper(get_the_title()); ?></b>
          </div><?php */?>
        </div>
      </div>
    </div>
    <?php
    if(have_posts()): while(have_posts()): the_post();
    ?>
    <div class="row">
      <div class="col-sm-6">
        <h2 class="domobrand-title"><?php Print strtoupper(get_the_title()); ?></h2><?php /*?><img class='brand-profile-logo' src='<?php Print get_field("logo")['sizes']['medium']; ?>' /><?php */?>
        <?php if(get_field('country')){ ?><div class="pt-2em"><strong><?php Print strtoupper(get_field("country")); ?></strong></div><?php } ?>
        <br><br>
        <?php the_content(); ?>
        <?php
        if (get_field("custom_range_url") != ""){
        ?>
          <div class="brandrange"><a href='<?php Print get_field("custom_range_url"); ?>' class='btn white-brown-outline-btn'>VIEW THIS RANGE</a></div>
        <?php } else { ?>
          <div class="brandrange"><a href='<?php Print get_site_url();?>/shop/?filter_brand=<?php global $post;Print $post->post_name; ?>' class='btn white-brown-outline-btn'>VIEW THIS RANGE</a></div>
        <?php } ?>

        <?php if( have_rows('brand_pdfs') ) :  ?>
          <div class="brochure-pdf-download btn-group">
            <a href="" type="button" class="btn white-brown-outline-btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">BROCHURE DOWNLOAD <i class="fa fa-angle-down ml-8"></i></a>
            <ul class="dropdown-menu">
              <?php while ( have_rows('brand_pdfs') ) : the_row(); ?>
                <?php $file = get_sub_field('upload_pdf'); ?>
                <li><a href="<?php echo $file['url']; ?>" target="_blank"><?php echo get_sub_field('title_for_pdf'); ?></a></li>
        <?php endwhile; ?>
            </ul>
          </div>
        <?php endif; ?>


        <?php
        /*if (get_field("brand_info_pdf") != null){
        ?>
        <a href='<?php Print get_field("brand_info_pdf"); ?>' class='btn white-brown-outline-btn'><i class="fa fa-file-text" aria-hidden="true"></i>BROCHURE  DOWNLOAD </a>

        <?php
      }*/
        ?>
        <br><br>
      </div>
      <div class="col-sm-6">
        <b><?php Print strtoupper(get_the_title()); ?> CRAFTSMANSHIP & INSPIRATION </b>
        <br><br>
        <div class="row">
          <?php
          $images = get_field("image_gallery");
          foreach($images as $image){
            ?>
            <div class='col-sm-4 brand-profile-gallery-image-holder'>
              <a href='<?php Print $image['url']; ?>'>
                <a href='<?php Print $image['sizes']['large'];?>' data-lightbox="brand">
                  <div class='brand-profile-gallery-image' style='background-image:url("<?php Print $image['sizes']['medium']; ?>");'></div>
                </a>
              </a>
            </div>
            <?php
          }
          ?>
        </div>
      </div>
    </div>
  <?php endwhile; endif; ?>
  </div>
</div>
<div class='additional_information'>
  <div class='additional_header'>
    <div class='container'>
      <h4>Additional Information</h4>
    </div>
  </div>
  <div class='brand-enquire'>
  <div class='container'>
    <div class='row'>
    	<div class="clearfix ptb-2em">
    		<div class='col-sm-6'>
      <p>ENQUIRE ABOUT THIS RANGE</p>
      <h3><?php Print strtoupper(get_the_title()); ?></h3>
      <div class='enquire-product-form brand-enquire-form'>
      <?php
      Print str_replace("%BRAND%",get_the_title(), do_shortcode("[contact-form-7 id='8534' title='Brand Enquiry']"));
      ?>
      </div>
    </div>
    		<div class="col-sm-6">
              <div class="become-domo-member">
                <header>BECOME A DOMO MEMBER & BE REWARDED</header>
                <small>BENEFITS INCLUDE...</small>
                <div class="row clearfix">
                    <div class="col-md-6">
                        <ul class="member-rewards">
                            <!--<li class="star">10% OFF FOR MEMBERS <span>FOR ONLINE PRODUCTS ONLY</span></li> -->
                            <li class="tag">EXCLUSIVE NOTIFICATION OF SALES & PROMOTIONS</li>
                            <li class="comment">DOMO NEWSLETTER EMAILED DIRECT TO YOU</li>
			                      <li class="ticket">INVITATIONS TO DOMO EVENTS & PRODUCT LAUNCHES</li>
                        </ul>
                    </div>
                    <div class="col-md-6">
                        <div class="signupimage">
                            <div class="signup-bg" style="background-image: url(<?php bloginfo('template_url'); ?>/img/membership_img.jpg);"></div>
                            <div><a href="<?php echo get_permalink(210); ?>" class="signupnow">Sign up now</a></div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
    	</div>
  </div>
  </div>
</div>
<?php get_footer(); ?>
