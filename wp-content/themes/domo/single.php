<?php
/**
 * The Template for displaying all single posts.
 *
 * @package _domo
 */

get_header(); ?>
<div class="content ptb-2em">
<div class="container">
	<div class="row clearfix">
        <div class="single-left col-sm-8 col-md-9 mmb-2em">
            <div class="blog-left-column">
                <?php while ( have_posts() ) : the_post(); ?>
                    <?php setPostViews(get_the_ID()); // this is used to set the count for popular posts ?>
                    <?php get_template_part( 'content', 'single' ); ?>
                    
                    <?php $gallery_images = get_field('news_gallery'); if($gallery_images): ?>
                    
                    <div class="extra_gallery_slider owl-carousel">
                    	<?php foreach ($gallery_images as $images){ ?>
                            <div class="gallery_items">
                                <a href="<?php echo $images['url']; ?>" data-lightbox="roadtrip"><figure style="background-image: url(<?php echo $images['sizes']['medium']; ?>)"></figure></a>
                            </div>
						<?php } ?>
                    </div>
                    <?php endif; ?>

                    <div class="share-and-comments pb-2em">
                        <div class="row clearfix">
                            <div class="col-xs-8 col-sm-6 col-md-6 back-to-news"><a href="<?php echo get_permalink(233); ?>" class="back_button"><i class="fa fa-angle-left mr-8"></i>Back to all news</a></div>
                            <div class="col-xs-4 col-sm-6 col-md-2 comment-circle">
                                <div class="comment_blox_circle">
                                    <span class="comment_number"><?php $my_var = get_comments_number( $post_id );  echo $my_var; ?></span>
                                    <span class="comment_txt">
                                        <?php if($my_var < 2) { echo "COMMENT"; } else { echo "COMMENTS"; } ?>
                                    </span>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-4 share_it_online">
                                <strong>Did you love this news article?</strong>
                                <span>Share it online</span>
                                <div class="social_icons pt-1em">
                                    <a href="http://www.facebook.com/share.php?u=<?php print(urlencode(get_permalink())); ?>&title=<?php print(urlencode(the_title())); ?>" target="_blank" class="fb"><i class="fa fa-facebook"></i></a>
                                    <a href="http://twitter.com/intent/tweet?status=<?php print(urlencode(the_title())); ?>+<?php print(urlencode(get_permalink())); ?>" target="_blank" class="tt"><i class="fa fa-twitter"></i></a>
                                    <!-- <a href="" class="ig"><i class="fa fa-instagram"></i></a> -->
                                    <a href="http://pinterest.com/pin/create/bookmarklet/?media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' ); print(urlencode($url = $thumb['0']));?>
                    &url=<?php print(urlencode(the_permalink())); ?>&is_video=false&description=<?php print(urlencode(get_the_title())); ?>" target="_blank" class="pt"><i class="fa fa-pinterest"></i></a>
                                    <a href="mailto:?subject=<?php print(urlencode(the_title())); ?>&body=Look at this...<?php print(urlencode(get_permalink())); ?>" class="el"> <i class="fa fa-envelope"></i> </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php //_domo_content_nav( 'nav-below' ); ?>
                    <?php
                        if ( comments_open() || '0' != get_comments_number() )
                            comments_template();
                    ?>
                <?php endwhile; // end of the loop. ?>
            </div>

            <div class="you_may_like">
                <h3 class="ptb-2em">YOU MAY ALSO LIKE...</h3>
                <div class="other_blog_post row clearfix">

                    <?php
                    $currentID = get_the_ID();
                    $my_query = new WP_Query( array('showposts' => '4', 'orderby' => 'rand', 'post__not_in' => array($currentID)));



                    while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
                        <?php $thumb_id = get_post_thumbnail_id();
                        $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'blog_thumb', true);
                        $thumb_url = $thumb_url_array[0]; ?>
                        <div class="col-md-6">
                            <article class="clearfix">
                                <a href="<?php the_permalink(); ?>"><span style="background-image: url('<?php echo $thumb_url; ?>');"></span></a>
                                <h4><a href="<?php the_permalink(); ?>"><?php $post_title = get_the_title(); echo substr($post_title, 0, 60);?> </a></h4>
                                <a href="<?php the_permalink(); ?>" class="read_more_btn_outline">read</a>
                            </article>




                        </div>
                    <?php endwhile;
										wp_reset_postdata();
										 ?>


                    <?php /*?><div class="col-md-6">
                        <article class="clearfix">
                            <figure><img src="<?php bloginfo('template_url'); ?>/img/blog_thumb.jpg" alt=""></figure>
                            <h4><a href="">Blog heading to go here, 60 characters max. inc. spaces.</a></h4>
                            <a href="" class="read_more_btn_outline">read</a>
                        </article>
                    </div>
                    <div class="col-md-6">
                        <article class="clearfix">
                            <figure><img src="<?php bloginfo('template_url'); ?>/img/blog_thumb.jpg" alt=""></figure>
                            <h4><a href="">Blog heading to go here, 60 characters max. inc. spaces.</a></h4>
                            <a href="" class="read_more_btn_outline">read</a>
                        </article>
                    </div>
                    <div class="col-md-6">
                        <article class="clearfix">
                            <figure><img src="<?php bloginfo('template_url'); ?>/img/blog_thumb.jpg" alt=""></figure>
                            <h4><a href="">Blog heading to go here, 60 characters max. inc. spaces.</a></h4>
                            <a href="" class="read_more_btn_outline">read</a>
                        </article>
                    </div>
                    <div class="col-md-6">
                        <article class="clearfix">
                            <figure><img src="<?php bloginfo('template_url'); ?>/img/blog_thumb.jpg" alt=""></figure>
                            <h4><a href="">Blog heading to go here, 60 characters max. inc. spaces.</a></h4>
                            <a href="" class="read_more_btn_outline">read</a>
                        </article>
                    </div><?php */?>
                </div>
            </div>

        </div>
        <?php get_sidebar(); ?>
	</div>
</div>
</div>
<?php get_footer(); ?>
