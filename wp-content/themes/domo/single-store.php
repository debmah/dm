<?php
get_header();
?>
<div class="content ptb-2em">
  <div class="container">
    <div class='store-location-card'>
      <div class='row'>
        <div class='col-sm-6'>
          <div class='row'>
            <div class='col-sm-12'>
              <div class='row'>
                <div class='col-sm-12'>
                  <h2> DOMO  <?php Print strtoupper(get_the_title()); ?> </h2>
                  <p class='store-state'><?php Print get_field("state"); ?></p>
                  <p class='phone-number'>
                    <a href='tel:<?php Print get_field("phone_number"); ?>'><i class='icon fa fa-phone'></i><?php Print get_field("phone_number"); ?></a>
                  </p>
                </div>
              </div>
              <br>
              <div class='row'>
                <div class='col-sm-6'>
                  <i class="icon fa fa-map-marker" aria-hidden="true"></i> <?php Print get_field("address"); ?>
                  <br>
                </div>
                <div class='col-sm-6'>
                  <?php
                  if (get_field("parking_information") != ""){
                    ?>
                    <p>
                      <i class="icon fa fa-car" aria-hidden="true"></i> <small>Parking Info</small>
                      <br>
                      <?php Print get_field("parking_information"); ?>
                    </p>
                    <?php
                  }

                  ?>
                  <br>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <p>
                    <i class="icon fa fa-clock-o" aria-hidden="true"></i>
                    <small>OPENING HOURS</small>
                  </p>
                  <p class='indent'>
                    <?php
                    $days = get_field("opening_hours");
                    foreach ($days as $day){
                      Print "<small>" . $day['days'] . "</small><br>";
                      Print $day['hours'] . "<br><br>";
                    }
                    ?>
                  </p>
                </div>
                <div class="col-sm-6">
                  <?php
                  if (get_field("fax_number") != ""){
                    ?>
                    <p>
                      <i class="icon fa fa-fax" aria-hidden="true"></i> <small>Fax</small>
                      &nbsp; <?php Print get_field("fax_number"); ?>
                    </p>
                    <?php
                  }
                  ?>
                  <br>
                  <?php
                  if (get_field("head_office") == true){
                    ?>
                    <p>
                      <i class="icon fa fa-certificate" aria-hidden="true"></i> <small>HQ</small>
                      &nbsp; DOMO Head Office
                    </p>
                    <?php
                  }
                  ?>
                  <?php
                  if (get_field("warehouse") == true){
                    ?>
                    <p>
                      <i class="icon fa fa-truck" aria-hidden="true"></i>
                      &nbsp; DOMO Warehouse
                    </p>
                    <?php
                  }
                  ?>
                  <?php

                  if (get_field("additional_information") != ""){
                    ?>
                    <br>
                    <?php Print get_field("additional_information"); ?>
                    <?php
                  }
                  ?>
                </div>
              </div>
              <br>
              <div class='row'>
                <div class='col-sm-6'>
                  <?php
                  if (get_field("payment_info") != ""){
                    ?>
                    <p>
                      <i class="icon fa fa-usd" aria-hidden="true"></i> <small>Payment Information</small>
                      <br>
                      <?php Print get_field("payment_info"); ?>
                    </p>
                    <br><br>
                    <?php
                  }
                  ?>
                </div>
                <div class='col-sm-6'>
                  <?php
                  if (get_field("interior_designer_info") != ""){
                    ?>
                    <p class='store-skill-holder'>
                      <small><?php Print get_field("skill_title"); ?></small>
                      <br>
                      <?php Print get_field("interior_designer_info"); ?>
                    </p>
                    <br><br>
                    <?php
                  }
                  ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class='col-sm-6'>
          <h2>Store Profile</h2>
          <p>
            <?php Print get_field("store_profile"); ?>
          </p>
          <br><br>
          <?php if (get_field("email") != ""){ ?>
          <a href='mailto:<?php Print get_field("email"); ?>' class='btn btn-brown btn-thin-wide'><strong>EMAIL THIS STORE</strong></a>
          <br><br>
          <?php
          }
          ?>
          <a target="_blank" href='https://maps.google.com?daddr=<?php Print get_field("latitude"); ?>,<?php Print get_field("longitude");?>' class='btn btn-brown btn-thin-wide'><strong>GET DIRECTIONS</strong></a>
          <br><br>
        </div>
      </div>
      <div class='row'>
        <div class='col-sm-6'>
          <div class='row'>
          <?php
          $images = get_field("image_gallery");
          foreach($images as $image){
            ?>
            <div class='col-sm-6'>
              <div class='shop-profile-image' style='background-image:url("<?php Print $image['sizes']['medium']; ?>")'></div>
            </div>
            <?php
          }
          ?>
          </div>
        </div>
        <div class='col-sm-6'>
          <div class='google-map' data-lat="<?php Print get_field('latitude'); ?>" data-lng=<?php Print get_field('longitude'); ?>></div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
get_footer();
?>
