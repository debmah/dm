<footer>
  <div class="quick-links-bar text-center">
    <div class="container">
      <div class="row clearfix">
        <div class="col-xs-3 col-md-3"><a href="#" style="cursor:default"><i class="fa fa-lock mr-8" aria-hidden="true"></i> <?php Print get_field("10_off_text", 17); ?></a></div>
        <div class="col-xs-3 col-md-3"><a href="<?php Print get_site_url(); ?>/sign-up"><i class="fa fa-diamond mr-8" aria-hidden="true"></i> become a member</a></div>
        <?php
        $wishlist_count = YITH_WCWL()->count_products();
        ?>
        <div class="col-xs-3 col-md-3"><a href="<?php echo get_permalink(8708); ?>"><i class="fa fa-heart-o mr-8" aria-hidden="true"></i> items in wishlist <strong class='ajax-wishlist-count'><?php Print $wishlist_count; ?></strong></a></div>
        <div class="col-xs-3 col-md-3"><a href="<?php Print get_site_url(); ?>/stores"><i class="fa fa-map-marker mr-8" aria-hidden="true"></i> store locations</a></div>
      </div>
    </div>
  </div>
  <div class="main-footer pt-3em pb-1em">
    <div class="container">
      <div class="row clearfix">
        <div class="col-sm-3 mmb-3em store_location_link">
          <figure><a href="<?php Print get_site_url(); ?>/stores"><img src="<?php bloginfo('template_url'); ?>/img/storelocations.jpg" alt="Store Locations"></a></figure>
        </div>
        <div class="col-sm-9 footerlinks">
          <div class="row clearfix">
            <?php
            wp_nav_menu(array('walker' => new Nav_Custom_Footer_Walker(), 'container'=>false, 'menu_class' => 'footerlinks', 'theme_location'=>'footer', 'fallback_cb'=>false ));
            ?>
          </div>
        </div>
      </div>
      <div class="social-links text-right"> <a href="<?php Print get_field("facebook_url", 141); ?>" target="_blank" class="facebook_url"><i class="fa fa-facebook"></i></a> <a href="<?php Print get_field("instagram_url", 141); ?>" target="_blank" class="instagram_url"><i class="fa fa-instagram"></i></a> </div>
      <div class="copyright-section text-center">
        <p><?php Print get_field("10_off_text_note", 17); ?>  Safe and secure payments  &copy;  <?php Print get_field("company_name", 17);?> <?php echo date('Y'); ?></p>
        <figure class="pt-1em"><img src="<?php bloginfo('template_url'); ?>/img/payment.jpg" alt="payment methods"></figure>
        <div class="site_by">Website by <a href="http://foxandlee.com.au" target="_blank">Fox & Lee</a></div>
      </div>
    </div>
  </div>
</footer>

<?php wp_footer(); ?>
<script>window.$ = jQuery;</script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/product-filter.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyAofRgnGrrt0_xywAOaaUQ4qO52a7ceI2c"></script>
<script src="<?php bloginfo('template_url'); ?>/js/custom.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 865207745;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/865207745/?guid=ON&amp;script=0"/>
</div>
</noscript>
</body>
</html>
