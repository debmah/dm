<?php get_header();
/* Template Name: Career */
?>
<div class="content pt-1em pb-3em">
    <div class="container">
        <div class="inner-full-width archive-career">
        	<div class="row clearfix">
            	<div class="col-md-7 career_content">
                	<?php while(have_posts()):the_post(); ?>
                    	<?php the_content(); ?>
                    <?php endwhile; ?>
                    <span class="followus">Follow and like us online</span>
                    <div class="social-links pb-3em"> <a href="<?php Print get_field("facebook_url", 141); ?>" target="_blank" class="facebook_url"><i class="fa fa-facebook"></i></a> <a href="<?php Print get_field("instagram_url", 141); ?>" target="_blank" class="instagram_url"><i class="fa fa-instagram"></i></a> </div>
                </div>
                <div class="col-md-5 career_form">
                	<div class="quick_career_form">
                    	<header class="text-center">work at domo</header>
                        <?php echo do_shortcode('[contact-form-7 id="8608" title="Career Form"]'); ?>
                    </div>
                </div>
            </div>


            <div class="positions-available">
            	<h3>CURRENT POSITIONS AVAILABLE</h3>
            	<div class="vacancy-lists">

                	<?php
					$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
					$temp = $wp_query;
					$wp_query = null;
					$wp_query = new WP_Query();
					$wp_query -> query('post_type=career&showposts=10'.'&paged='.$paged);
					while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

						<article>
                            <div class="row clearfix">
                                <div class="col-sm-8 col-md-8 col-lg-9 jd">
                                    <h4><?php the_title(); ?></h4>
                                    <p><?php echo get_field('job_category'); ?></p>
                                    <p><strong>Date Listed</strong><?php echo get_field('date_listed'); ?></p>
                                    <p><strong>Location</strong><?php echo get_field('job_location'); ?></p>
                                    <p><strong>Job Type</strong><?php echo get_field('job_type'); ?></p>
                                    <p>
                                        <strong>About the role</strong>
                                        <?php echo get_field('about_the_role'); ?>
                                    </p>
                                </div>
                                <div class="col-sm-4 col-md-4 col-lg-3">
                                    <div class="apply_now_box">
                                        <div class="applycontent">
                                            <header>APPLY FOR THIS<br>POSITION</header> 
                                            <a href="<?php echo get_field('url_to_link_the_job'); ?>" target="_blank" class="view_more_btn">Apply Now</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>

					<?php endwhile; ?>
                     <!-- Pagination Nav -->
                    <div class="pagination-nav">
                         <?php the_posts_pagination( $args ); ?>
                        <?php $wp_query = null; $wp_query = $temp; ?> <!-- this is to clear wp_query -->
                    </div>
                    <!-- End Pagination Nav -->


                </div>
            </div>


        </div>
    </div>
</div>
<?php get_footer(); ?>
