<?php get_header(); 
// This is about domo page
?>

<div class="content pt-1em pb-3em">
    <div class="container">
        <div class="inner-full-width wishlist-page">
        	<h3 class="wishlist-titlehead">YOUR DOMO WISHLIST</h3>
        	<div class="row clearfix">
            	<div class="col-md-3 wishlist_left_sidebar pb-3em">
                	<?php dynamic_sidebar( 'sidebar-1' ); ?>
                </div>
                <div class="col-md-9 wishlist_dtls">
                	<?php while(have_posts()):the_post(); ?>
                    	<?php the_content(); ?>
                    <?php endwhile; ?>
                </div>
            </div>
            
          
            
        </div>
    </div>
</div>

<?php get_footer(); ?>
