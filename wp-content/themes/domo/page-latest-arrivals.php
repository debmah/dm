<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'alt' ); ?>

<div class="content ptb-2em">
	<div class="container">
    <div class='row'>
      <div class='col-sm-12'>
        <div class='large-page-lifestyle-image' style='background-image: url("<?php Print get_field("image")['sizes']['large']; ?>")'>
          <div class='latest-arrivals-banner-text'>
            <h2 class='bold'><?php Print get_field("header_line_1"); ?></h2>
            <h2 class='light'><?php Print get_field("header_line_2"); ?></h2>
          </div>
        </div>
      </div>
    </div>
		<div class='row product-filter-row'>

			<script>
			function createProductFilters(){
        //No filters need to be created
			}

			function createProducts(){
				var template = "";
				template += "<div class='col-sm-6 col-md-4 col-lg-3 product-filter-item' data-brand='{{brand}}' data-style='{{style}}' data-product-cat='{{product_cat}}' data-upholstery='{{upholstery}}' data-domo-product-type='{{domo_product_type}}'>";
				template += "	<a href='{{link}}'>";
				template += "  <div class='domo-product'>";
				template += "   <div class='image'>{{image}}</div>";
				template += "   <span class='product-title'>{{name}}</span>";
				template += "   <span class='product-brand'>{{brand_name}}</span>";
				template += "   <div class='product-tags'>{{sale}}{{promotion}}{{clearance}}</div>";
				template += "  </div>";
				template += " </a>";
				template += "</div>";
				ProductFilter.setProductTemplate(template);

				<?php
				$cached_products = get_transient("latest_arrivals_products_cache");

				if ($cached_products){
					?>
					ProductFilter.setProducts("<?php Print base64_encode(gzcompress($cached_products)); ?>", function(){
						ProductFilter.search("");
						ProductFilter.search("<?php Print $_GET['search']; ?>");
					});
					<?php
				} else {
					?>
					ProductFilter.setProducts([]);
					<?php
				}
				?>
			}

			</script>

		</div>
		<?php if ($cached_products == "[]" || $cached_products == null){
			?>
			<div class='no-products-found'>
				<b>There are currently no latest arrivals</b>
				<p>Click below to view our full furniture range</p>
				<a href='<?php Print get_site_url(); ?>/shop' class='button'>DOMO FURNITURE RANGE</a>
			</div>
			<?php
		} ?>
		<div class='row product-holder'>
		</div>
	</div>
</div>
<?php get_footer( 'alt' ); ?>
