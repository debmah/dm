
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php /*?><header>
		<h1 class="page-title"><?php the_title(); ?></h1>
        </header><?php */?>

		<?php /*?><div class="entry-meta">
			<?php _domo_posted_on(); ?>
		</div><?php */?><!-- .entry-meta -->
	<!-- .entry-header -->

	<div class="entry-content">
		<div class="entry-content-thumbnail">
			<?php the_post_thumbnail(); ?>
		</div>
        <header class="pageheader"><h1 class="page-title"><?php the_title(); ?></h1><div class="posted_date_month"><span class="current_post_date"><?php $date_store = date("d", strtotime($post->post_date)); echo ltrim($date_store, 0); ?></span><span class="current_post_month text-uppercase"><?php echo date("M", strtotime($post->post_date)); ?></span></div></header>
        <div class="post-meta"><small>POSTED BY DOMO</small><small class="posted-date"><?php echo date("d M Y", strtotime($post->post_date)); ?></small></div>
        <div class="related_post_cat clearfix">
			<?php  $category = get_the_category();
			foreach ($category as $outpuvalue){
				echo "<span><a href='" . get_category_link( $outpuvalue->term_id ) . "'>" . $outpuvalue->cat_name . "</a></span>";
            }
            ?>
		</div>

        <div class="pt-2em">
			<?php the_content(); ?>
        </div>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
