<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="theme-color" content="#614F49">
<title>
<?php wp_title( '|', true, 'right' ); ?>
</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
<link href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap-off-canvas-nav.css">
<link href="<?php bloginfo('template_url'); ?>/css/owl.carousel.css" rel="stylesheet">
<!--<link href="<?php bloginfo('template_url'); ?>/css/style.css" rel="stylesheet">-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" />
<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/img/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="<?php bloginfo('template_url'); ?>/img/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="<?php bloginfo('template_url'); ?>/img/favicon/manifest.json">
<link rel="mask-icon" href="<?php bloginfo('template_url'); ?>/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css" />
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WGMN7K8');</script>
<!-- End Google Tag Manager -->
</head>

<body <?php body_class('off-canvas-nav-left'); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WGMN7K8"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php do_action( 'before' ); ?>
<div class="darkbg"></div>
<header class="main-header">
  <div class="top-header ptb-2em hidden-sm hidden-xs">
    <div class="container pos-relative">
      <div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php bloginfo('template_url'); ?>/img/domo-logo-master.svg" alt="domo" width="235"></a></div>
      <div class="top-right-block">
        <ul>
          <?php if (!is_user_logged_in()){ ?>
          <li class="sign-in login_signup"><a href="#">Sign In</a></li>
          <?php
            } else {
            ?>

          <li class="sign-in"><a href="#">My Account</a>
            <ul class="account-dtls">
              <li><a href="<?php echo get_permalink(8708); ?>">Wishlist</a></li>
              <li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">Account Settings</a></li>
              <li><a href="<?php echo wp_logout_url(get_permalink()); ?>">Log out</a></li>
            </ul>
          </li>
          <?php } ?>
          <?php //$url = YITH_WCWL()->get_wishlist_url(); ?>
          <?php
          $wishlist_count = YITH_WCWL()->count_products();
          ?>
          <li class="wishlist-count"><a href="<?php echo get_permalink(8708); ?>"><?php if($wishlist_count>0){ ?><i class="fa fa-heart" aria-hidden="true"></i> <?php } else { ?><i class="fa fa-heart-o" aria-hidden="true"></i><?php } ?><small class='ajax-wishlist-count'><?php Print $wishlist_count; ?></small></a></li>
          <li class="cart-count"><a href="<?php echo wc_get_cart_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/img/shop-cart-bag.svg" alt="shopping cart" width="14" class="mr-8"></a><a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo sprintf ( _n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?> </a></li>
        </ul>





      </div>
    </div>
  </div>
  <nav class="navbar custom_nav">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header nav-mobile-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        <a class="navbar-brand hidden" href="#">Domo</a> <a href="#showsearch" class="search-icon-mobile visible-sm visible-xs"><i class="fa fa-search"></i></a>
        <div class="logo_for_mobile visible-sm visible-xs"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php bloginfo('template_url'); ?>/img/domo-logo-stacked.svg" alt="domo" width="111"></a></div>
      </div>
      <div class="mobile_search clearfix" id="showsearch" style="display:none">
        <form action="<?php Print get_site_url(); ?>/shop" method="get" role="Search" class="navbar-form">
          <fieldset>
            <input type="search" name="search" placeholder="Search">
          </fieldset>
        </form>
      </div>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <?php
            wp_nav_menu(array('walker' => new Nav_Custom_Walker(), 'container'=>false, 'menu_class' => 'nav navbar-nav', 'theme_location'=>'primary', 'fallback_cb'=>false ));
            ?>
        <form action="<?php Print get_site_url(); ?>/shop" method="get" class="navbar-form navbar-left header-search hidden-sm hidden-xs" role="search">
          <div class="form-group">
            <input type="text" name="search" class="form-control" placeholder="Search">
          </div>
        </form>
      </div>
    </div>
      </nav>
      <div class="sign-in-box ptb-3em hidden-sm hidden-xs">
        <div class="container">
          <div class="row clearfix">
            <div class="col-sm-6 log_in_section">
              <h3>RETURNING CUSTOMERS</h3>
              <div class="login-section">
                <?php Print do_shortcode("[domo_login]"); ?>
              </div>
            </div>
            <div class="col-sm-6 sign_up_section">
              <h3>JOIN DOMO & BE REWARDED</h3>
              <p>Member benefits include ...</p>
              <ul class="signup_member_benefits">
                <!--<li><i class="fa fa-star mr-8"></i>10% OFF FOR MEMBERS, FOR ONLINE PURCHASES ONLY</li>-->
                <li><i class="fa fa-tag mr-8"></i>DISCOUNT OFFERS & EXCLUSIVE SALES</li>
                <li><i class="fa fa-comment mr-8"></i>DOMO RETAIL NEWSLETTER</li>
                <li><i class="fa fa-file-text mr-8"></i>EXCLUSIVE TRADE NEWSLETTER</li>
              </ul>
              <div class="joindomo"><a href="<?php echo get_site_url(); ?>/sign-up" class="view_more_btn">Join DOMO</a></div>
            </div>
          </div>
          <div class="closebox"><a href=""><i class="fa fa-times-circle" aria-hidden="true"></i></a></div>
        </div>
      </div>

  <!-- sign in box -->
</header>
<!-- header -->




<?php if(is_front_page()) { ?>
<div class="banner">
  <div class="container">
    <div class="banner-slide">
      <?php
            if( have_rows('slides') ):
              while ( have_rows('slides') ) : the_row(); ?>
              <div>
      <div class="banner-items" style="background-image: url('<?php echo get_sub_field("image"); ?>');"></div>
      <div class="banner-info">
        <h1><?php echo get_sub_field('heading_line_1'); ?></h1>
        <h2><?php echo get_sub_field('heading_line_2'); ?> </h2>
        <p><?php //echo get_sub_field('subheading'); ?> <span><a href="<?php echo get_sub_field('button_link'); ?>" class="rounded_btn border_only button"><?php echo get_sub_field('button_text'); ?> <i class="fa fa-angle-right ml-8"></i></a></span></p>
        <div class="triangle-bottomright"></div>
      </div>
    </div>
      <?php endwhile; endif; ?>
    </div>
  </div>
</div>
<!-- banner -->
<?php } else { ?>
<div class="header_titles_breadcrumbs">
  <div class="container">
    <div class="row clearfix">
      <div class="col-sm-7 col-md-7">
        <?php
                if (is_singular('product')){
                  $shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
				  $url = htmlspecialchars($_SERVER['HTTP_REFERER']);
                  ?>
        <a href='<?php Print $url; ?>' class='brown-outline-btn'><i class='fa fa-chevron-left'></i> &nbsp;&nbsp; Back to Products</a>
        <?php
                }

                if (is_singular('brand_profile')){
                  ?>
        <h1 class='page-title'>
          <?php the_title(); ?>
        </h1>
        <?php
                }

                if (is_singular(array('store', 'domo_stockist'))){
                  $store_page_url = get_site_url() . "/stores";
                  ?>
        <a href='<?php Print $store_page_url; ?>' class='brown-outline-btn'><i class='fa fa-chevron-left'></i> &nbsp;&nbsp; Back to All Stores</a>
        <?php
                }
                ?>
        <?php if(is_archive() && !is_post_type_archive('product')) { ?>
        <h1 class="page-title">
          <?php
                    $store_location_name = "";
                    if (isset($_GET['state'])){
                      $term = get_term_by( 'slug', $_GET['state'], 'state' );
                      $store_location_name = " - " . $term->name;
                    }

                    if ( is_category() ) :
                      single_cat_title();

                      elseif ( is_tag() ) :
                        single_tag_title();

                        elseif ( is_author() ) :
                          /* Queue the first post, that way we know
                          * what author we're dealing with (if that is the case).
                          */
                          the_post();
                          printf( __( 'Author: %s', '_fox' ), '<span class="vcard">' . get_the_author() . '</span>' );
                          /* Since we called the_post() above, we need to
                          * rewind the loop back to the beginning that way
                          * we can run the loop properly, in full.
                          */
                          rewind_posts();

                          elseif ( is_day() ) :
                            printf( __( 'Day: %s', '_fox' ), '<span>' . get_the_date() . '</span>' );

                            elseif ( is_month() ) :
                              printf( __( 'Month: %s', '_fox' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );

                              elseif ( is_year() ) :
                                printf( __( 'Year: %s', '_fox' ), '<span>' . get_the_date( 'Y' ) . '</span>' );

                                elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
                                  _e( 'Asides', '_fox' );

                                  elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
                                    _e( 'Images', '_fox');

                                    elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
                                      _e( 'Videos', '_fox' );

                                      elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
                                        _e( 'Quotes', '_fox' );

                                        elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
                                          _e( 'Links', '_fox' );

                                          elseif ( is_post_type_archive('store') ) :
                                            _e( 'Store Locations' . $store_location_name, '_fox' );

                                            elseif ( is_tax('product_cat') ) :
                                              _e( single_term_title(), '_fox' );

                                            else :
                                              _e( 'Archives', '_fox' );

                                            endif;
                                            ?>
        </h1>
        <?php } else { ?>
        <?php
                                            $title = get_the_title();
                                            //echo $title;
                                            if (is_404()){
                                              $title = "404 ERROR MESSAGE";
                                            };
                                            if (is_singular()) { $title = ''; }
                                            if (is_archive('product')){ $title = "Products"; }
                                            if (is_archive('product') && isset($_GET['search'])){ $title = "<span class='search-results-title'>Search results: " . $_GET['search'] . "<i onclick='ProductFilter.clearSearch()' class='fa fa-times-circle' aria-hidden='true'></i></span>";}
                                            ?>
        <?php if(is_page()) { echo "<h1 class=\"page-title\">" . get_the_title() . "</h1>"; }?>
        <h1 class="page-title"><?php echo $title; ?></h1>
        <?php } ?>
        <?php if(is_singular('post') ) { echo "<a class='back_to_news read_more_btn_outline' href='" . get_permalink(233). "'><i class='fa fa-angle-left mr-8'></i>Back to all news</a>"; } ?>
        <?php if(is_singular('tip') ) { echo "<a class='back_to_news read_more_btn_outline' href='" . get_permalink(8573). "'><i class='fa fa-angle-left mr-8'></i>Back to care & usage</a>"; } ?>
        <?php if(is_singular('promotion') ) { echo "<a class='back_to_news read_more_btn_outline' href='" . get_permalink(8733). "'><i class='fa fa-angle-left mr-8'></i>Back to promotions</a>"; } ?>
      </div>
      <div class="col-sm-5 col-md-5">
        <div class="breadcrumbs text-right" typeof="BreadcrumbList" vocab="http://schema.org/">
          <?php if(function_exists('bcn_display')) { bcn_display(); } ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
