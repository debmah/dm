<?php
get_header();
/* Template Name: Resources */
?>


<div class="content ptb-1em">
	<div class="container">
    	<div class="inner-full-width resource_download_section">
        	<div class="info_and_logout pb-2em"><p>The below resources are available for you to download. <?php/*<span><a href="<?php echo wp_logout_url(); ?>" class="view_more_btn">Log out</a></span>*/?></p></div>
            <div class="downloads-resources-section">
            	<?php
				$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
				$temp = $wp_query;
				$wp_query = null;

				$args = array(
					'post_type' => 'resource',
					'numberposts' => 10,
					'paged' => $paged
				);

				$wp_query = new WP_Query($args);
				while ($wp_query->have_posts()) : $wp_query->the_post();
				$thumb_id = get_post_thumbnail_id();
				$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
				$thumb_url = $thumb_url_array[0]; ?>
                <?php $img = get_field('preview_image'); ?>


				<article class="clearfix">
                	<figure style="background-image:url('<?php echo $img['url']; ?>');"></figure>
                    <h2><?php the_title(); ?></h2>
                    <?php the_content(); ?>
                    <a href="<?php echo get_field('file'); ?>" target="_blank" class="view_more_btn">download</a>
                </article>

                <?php endwhile; ?>


                <!-- Pagination Nav -->
                <div class="pagination-nav">
                     <?php the_posts_pagination( $args ); ?>
                    <?php $wp_query = null; $wp_query = $temp; ?> <!-- this is to clear wp_query -->
                </div>
                <!-- End Pagination Nav -->




            </div>
        </div>
    </div>
</div>


<?php get_footer(); ?>
