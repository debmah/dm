<?php get_header();
/* Template Name: Sign Up*/

/* Define some variables */
$form_error = "";
$error = false;
$success = false;
$successType = "retail";
$selectedTab = "retail";

/* Check whether or not the form has been submitted */
if (isset($_POST['type']) && $_POST['type'] === "retail"){
	$selectedTab = "retail";
	$first_name = trim($_POST['first_name']);
	$last_name = trim($_POST['last_name']);
	$email = trim($_POST['email']);
	$postcode = trim($_POST['postcode']);
	$password = $_POST['password'];
	$confirm_password = $_POST['confirm_password'];
	$accept_terms = $_POST['agree_terms'];

	if ($form_error === "" && $first_name === ""){
		$form_error = "Please enter a first name";
		$error = true;
	}

	if ($form_error === "" && $last_name === ""){
		$form_error = "Please enter a last name";
		$error = true;
	}

	if ($form_error === "" && $email === ""){
		$form_error = "Please enter an email address";
		$error = true;
	}

	if ($form_error === "" && $postcode === ""){
		$form_error = "Please enter your post code";
		$error = true;
	}

	if ($form_error === "" && $password === ""){
		$form_error = "Please enter a password";
		$error = true;
	}

	if ($form_error === "" && $confirm_password !== $password){
		$form_error = "Passwords don't match";
		$error = true;
	}

	if ($form_error === "" && $accept_terms !== "on"){
		$form_error = "Please accept the terms and conditions to continue";
		$error = true;
	}

	if ($form_error === "" && email_exists($email)){
		$form_error = "Email address belongs to another user on this site.";
		$error = true;
	}

	if ($error === false){
		/* Create the user */
		$user_id = wp_create_user ( $email, $password, $email );
		$user = new WP_User( $user_id );
		$user->set_role( 'customer' );
		wp_update_user( array( 'ID' => $user_id, 'first_name' => $first_name, 'last_name' => $last_name ) );
		update_field("user_type", "retail", $user);
		update_user_meta($user_id, "shipping_postcode", $postcode);
		update_user_meta($user_id, "billing_postcode", $postcode);
		$success = true;
		$successType = "retail";
		$data = array(
			"firstName" => $first_name,
			"lastName" => $last_name,
			"userType" => "retail",
			"email" => $email,
			"postcode" => $postcode,
			"phone" => "",
			"company" => "",
		);

		update_user_meta($user_id, "user_data", serialize($data));

		//send_welcome_email($first_name, $email, "retail", $data);

		send_email_verification($user_id);
	}
}

if (isset($_POST['type']) && $_POST['type'] === "trade"){
	$selectedTab = "trade";
	$first_name = trim($_POST['first_name']);
	$last_name = trim($_POST['last_name']);
	$email = trim($_POST['email']);
	$postcode = trim($_POST['postcode']);
	$phone = trim($_POST['phone']);
	$company = trim($_POST['company']);
	$password = $_POST['password'];
	$confirm_password = $_POST['confirm_password'];
	$accept_terms = $_POST['agree_terms'];

	$showroom = $_POST['select_showroom'];

	if ($form_error === "" && $first_name === ""){
		$form_error = "Please enter a first name";
		$error = true;
	}

	if ($form_error === "" && $last_name === ""){
		$form_error = "Please enter a last name";
		$error = true;
	}

	if ($form_error === "" && $email === ""){
		$form_error = "Please enter an email address";
		$error = true;
	}

	if ($form_error === "" && $postcode === ""){
		$form_error = "Please enter your post code";
		$error = true;
	}

	if ($form_error === "" && $company === ""){
		$form_error = "Please enter a company name";
		$error = true;
	}

	if ($form_error === "" && $phone === ""){
		$form_error = "Please enter your phone number";
		$error = true;
	}

	if ($form_error === "" && $password === ""){
		$form_error = "Please enter a password";
		$error = true;
	}

	if ($form_error === "" && $confirm_password !== $password){
		$form_error = "Passwords don't match";
		$error = true;
	}

	if ($form_error === "" && $accept_terms !== "on"){
		$form_error = "Please accept the terms and conditions to continue";
		$error = true;
	}

	if ($form_error === "" && email_exists($email)){
		$form_error = "Email address belongs to another user on this site.";
		$error = true;
	}

	if ($form_error === "" && $showroom === ""){
		$form_error = "Please choose a showroom";
		$error = true;
	}

	if ($error === false){
		/* Create the user */
		$user_id = wp_create_user ( $email, $password, $email );
		$user = new WP_User( $user_id );
		$user->set_role( 'customer' );
		wp_update_user( array( 'ID' => $user_id, 'first_name' => $first_name, 'last_name' => $last_name ) );
		update_field("user_type", "trade", $user);
		update_user_meta($user_id, "shipping_postcode", $postcode);
		update_user_meta($user_id, "billing_postcode", $postcode);
		update_user_meta($user_id, "shipping_company", $company);
		update_user_meta($user_id, "billing_company", $company);
		update_user_meta($user_id, "shipping_phone", $phone);
		update_user_meta($user_id, "shipping_showroom", $showroom);

		$success = true;
		$successType = "trade";

		$data = array(
			"firstName" => $first_name,
			"lastName" => $last_name,
			"userType" => "retail",
			"email" => $email,
			"postcode" => $postcode,
			"phone" => $phone,
			"company" => $company,
			"showroom" => $showroom,
		);

		update_user_meta($user_id, "user_data", serialize($data));

		// send_welcome_email($first_name, $email, "trade", $data);
		send_email_verification($user_id);
	}
}
?>
<div class="content pt-1em pb-3em sign-up-template">
<div class="container">
	<div class="row clearfix">
    	<div class="col-sm-7">
        	<div class="signup_box mmb-2em">
            	<div class="newsletter_box">
                  <div class="newsletter-tab">
                    <div class="slidebutton">
                      <input type="checkbox" id="checkedbox" onchange="valueChanged()" <?php if ($selectedTab == "trade"){ Print "checked"; } ?>/>
                      <label for="checkedbox"> <!-- class="attention" -->
                        <i></i> </label>
                        <div class="pipe_line"></div>
                      </div>
                      <!-- /slidebutton -->
                      <div class="newsletter-form first" style="display:<?php if ($selectedTab == "trade"){ Print "none"; } else { Print "block"; } ?>">
												<?php
												if ($form_error != ""){
												?>
												<div class="alert alert-danger">
  													<strong>Error!</strong> <?php Print $form_error; ?>
												</div>
												<?php } ?>

												<?php
												if ($success){
												?>
												<div class="alert alert-success">
  													<strong>Congratulations you are almost a DOMO Member.</strong>
														<br />
														<?php
															if ($successType == "retail") {
														?>
														To finish registration please confirm your email address by clicking on the link in the email we have just sent you. Your membership will then be finalised.
														<?php
															} else {
														?>
														To finish registration please confirm your email address by clicking on the link in the email we have just sent you and completed the Contract registration forms sent in separate email.
														<?php
															}
														?>
												</div>
												<?php
												}
												?>
                        <form action="" method="post">
                          <fieldset class="row clearfix">
                            <div class="col-sm-6 firstname">
                              <input type="text" placeholder="First Name*" name="first_name" value="<?php if (isset($_POST['first_name']) && $success == false){Print $_POST['first_name']; } ?>">
                            </div>
                            <div class="col-sm-6 lastname">
                              <input type="text" placeholder="Last Name*" name="last_name" value="<?php if (isset($_POST['last_name']) && $success == false){Print $_POST['last_name']; } ?>">
                            </div>
                          </fieldset>
                          <fieldset class="row clearfix">
                            <div class="col-sm-6 firstname">
                              <input type="email" placeholder="Email Address*" name="email" value="<?php if (isset($_POST['email']) && $success == false){Print $_POST['email']; } ?>">
                            </div>
                            <div class="col-sm-6 lastname">
                              <input type="text" placeholder="Post code*" name="postcode" value="<?php if (isset($_POST['postcode']) && $success == false){Print $_POST['postcode']; } ?>">
                            </div>
                          </fieldset>
                          <fieldset class="row clearfix">
                            <div class="col-sm-6 firstname">
                              <input type="password" placeholder="Password*" name="password" value="<?php if (isset($_POST['password']) && $success == false){Print $_POST['password']; } ?>">
                            </div>
                            <div class="col-sm-6 lastname">
                              <input type="password" placeholder="Confirm Password*" name="confirm_password" value="<?php if (isset($_POST['confirm_password']) && $success == false){Print $_POST['confirm_password']; } ?>">
                            </div>
                          </fieldset>
                          <fieldset class="check_agreement">
                          	<input type="checkbox" id="agreed" name="agree_terms" <?php if (isset($_POST['agree_terms']) && $_POST['agree_terms'] == "on" && $success == false){Print "checked"; } ?>>
														<label class='fake-checkbox' for="agreed"></label>
														<label>Yes, I understand<br>
                            By becoming a DOMO member, you will be entered into our marketing database and will receive monthly information about member offers, furniture & accessory sales and other DOMO news.</label>
                          </fieldset>
													<br><br>
                          <fieldset class="text-center">
														<input type="hidden" name="type" value="retail">
                            <input type="submit" value="Create Account">
                          </fieldset>
                        </form>
                      </div>
                      <div class="newsletter-form second" style="display:<?php if ($selectedTab == "trade"){ Print "block"; } else { Print "none"; } ?>">
												<?php
												if ($form_error != ""){
												?>
												<div class="alert alert-danger">
  													<strong>Error!</strong> <?php Print $form_error; ?>
												</div>
												<?php } ?>

												<?php
												if ($success){
												?>
												<div class="alert alert-success">
													<strong>Congratulations you are almost a DOMO Member.</strong>
													<br />
													<?php
														if ($successType == "retail") {
													?>
													To finish registration please confirm your email address by clicking on the link in the email we have just sent you. Your membership will then be finalised.
													<?php
														} else {
													?>
													To finish registration please confirm your email address by clicking on the link in the email we have just sent you and completed the Contract registration forms sent in separate email.
													<?php
														}
													?>
												</div>
												<?php
												}
												?>
                        <form action="" method="post">
                          <fieldset class="row clearfix">
                            <div class="col-sm-6 firstname">
                              <input type="text" name="first_name" placeholder="First Name*" value="<?php if (isset($_POST['first_name']) && $success == false){Print $_POST['first_name']; } ?>">
                            </div>
                            <div class="col-sm-6 lastname">
                              <input type="text" name="last_name" placeholder="Last Name*" value="<?php if (isset($_POST['last_name']) && $success == false){Print $_POST['last_name']; } ?>">
                            </div>
                          </fieldset>
                          <fieldset class="row clearfix">
                            <div class="col-sm-6 firstname">
                              <input type="email" name="email" placeholder="Email*" value="<?php if (isset($_POST['email']) && $success == false){Print $_POST['email']; } ?>">
                            </div>
                            <div class="col-sm-6 lastname">
                              <input type="text" name="phone" placeholder="Phone*" value="<?php if (isset($_POST['phone']) && $success == false){Print $_POST['phone']; } ?>">
                            </div>
                          </fieldset>
                          <fieldset class="row clearfix">
                            <div class="col-sm-6 firstname">
                              <input type="text" name="company" placeholder="Company*" value="<?php if (isset($_POST['company']) && $success == false){Print $_POST['company']; } ?>">
                            </div>
                            <div class="col-sm-6 lastname">
                              <input type="text" name="postcode" placeholder="Post code*" value="<?php if (isset($_POST['postcode']) && $success == false){Print $_POST['postcode']; } ?>">
                            </div>
                          </fieldset>
                          <fieldset class="row clearfix">
                            <div class="col-sm-6 firstname">
                              <input type="password" name="password" placeholder="Password*" value="<?php if (isset($_POST['password']) && $success == false){Print $_POST['password']; } ?>">
                            </div>
                            <div class="col-sm-6 lastname">
                              <input type="password" name="confirm_password" placeholder="Confirm Password*" value="<?php if (isset($_POST['confirm_password']) && $success == false){Print $_POST['confirm_password']; } ?>">
                            </div>
                          </fieldset>
                          <fieldset class="clearfix">
                          	<div class="rounded-select-holder join_domo_store">
															<?php
															// Get a list of stores
												      $stores = get_posts(
												       array(
												         'posts_per_page' => -1,
												         'post_type' => 'store',
												        )
												      );
															?>
															<select name="select_showroom" class="quick-connect-selector">
																	<option value="">Preferred Showroom*</option>
																	<?php
																	foreach($stores as $store) {
																		?>
																		<option value="<?php Print $store->post_name; ?>"><?php Print $store->post_title; ?></option>
																		<?php
																	}
																	?>
															</select>
                         	</div>
                          </fieldset>
                          <fieldset class="check_agreement">
                          	<input type="checkbox" name="agree_terms" id="agreed_2" <?php if (isset($_POST['agree_terms']) && $_POST['agree_terms'] == "on" && $success == false){Print "checked"; } ?>>
														<label class='fake-checkbox' for="agreed_2"></label>
														<label>Yes, I understand<br>
                            By joining our DOMO Loyalty program you will receive an application form to complete. All brands that DOMO support can be used in a domestic or commercial environment. However, specific dedicated Contract/Trade Applications can be sourced from a number of key brands that we carry such as Ligne Roset, RODA and Point.</label>
                          </fieldset>
													<br>
                          <fieldset class="text-center">
														<input type="hidden" name="type" value="trade">
                            <input type="submit" value="Create Account">
                          </fieldset>
                        </form>
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-5">
        	<div class="member_benefits member-benefits first">
            	<h3>RETAIL MEMBER BENEFITS</h3>
							<br>
                <div class="benefit_lists">

									<!--<div class='row'>
										<article class="benefit_block">
										<div class='col-xs-2 text-right'>
											<span><i class="fa fa-star"></i></span>
										</div>
										<div class='col-xs-9'>
											<strong>10% OFF FOR REGISTERED MEMBERS</strong>
											<strong>ON CLICK & COLLECT PRODUCTS</strong>
										</div>
										</article>
									</div>-->

									<br>
									<div class='row'>
										<article class="benefit_block">
										<div class='col-xs-2 text-right'>
											<span><i class="fa fa-tag"></i></span>
										</div>
										<div class='col-xs-9'>
												<strong>EXCLUSIVE NOTIFICATION IN ADVANCE FOR SALES & PROMOTIONS</strong>
										</div>
										</article>
									</div>
									<br>
									<div class='row'>
										<article class="benefit_block">
										<div class='col-xs-2 text-right'>
											<span><i class="fa fa-ticket"></i></span>
										</div>
										<div class='col-xs-9'>
												<strong>INVITATIONS TO DOMO EVENTS & PRODUCT LAUNCHES </strong>
										</div>
										</article>
									</div>
									<br>
									<div class='row'>
										<article class="benefit_block">
										<div class='col-xs-2 text-right'>
											<span><i class="fa fa-comment"></i></span>
										</div>
										<div class='col-xs-9'>
												<strong>LIFESTYLE & TRENDS NEWSLETTER</strong>
												<strong>ALONG WITH TIPS FOR LIVING BEAUTIFULLY</strong>
										</div>
										</article>
									</div>
									<br>
                </div>
                <!--<div class="benefit_image pt-1em"><img src="<?php bloginfo('template_url'); ?>/img/memberbenefits.jpg" alt="Member Benefit"></div>-->
            </div>
						<div class="member_benefits member-benefits second">
	            	<h3>CONTRACT MEMBER BENEFITS</h3>
								<br>
	                <div class="benefit_lists">

                    	<div class="row">
                        	<article class="benefit_block">
                            	<div class='col-xs-2 text-right'>
                                    <span><i class="fa fa-diamond"></i></span>
                                </div>
                                <div class='col-xs-9'>
                                    <strong>EXCLUSIVE LOYALTY PROGRAM</strong>
                                </div>
                            </article>
                        </div>
                        <br>
                        <div class="row">
                        	<article class="benefit_block">
                            	<div class='col-xs-2 text-right'>
                                    <span><i class="fa fa-star"></i></span>
                                </div>
                                <div class='col-xs-9'>
                                    <strong>3 LEVEL DISCOUNTING STRUCTURE</strong>
                                </div>
                            </article>
                        </div>
                        <br>
                        <div class="row">
                        	<article class="benefit_block">
                            	<div class='col-xs-2 text-right'>
                                    <span><i class="fa fa-ticket"></i></span>
                                </div>
                                <div class='col-xs-9'>
                                    <strong>INVITE TO DOMO & INDUSTRY EVENTS</strong>
                                </div>
                            </article>
                        </div>
                        <br>
                        <div class="row">
                        	<article class="benefit_block">
                            	<div class='col-xs-2 text-right'>
                                    <span><i class="fa fa-certificate"></i></span>
                                </div>
                                <div class='col-xs-9'>
                                    <strong>BRAND & PRODUCT LAUNCHES</strong>
                                </div>
                            </article>
                        </div>
                        <br>
                        <div class="row">
                        	<article class="benefit_block">
                            	<div class='col-xs-2 text-right'>
                                    <span><i class="fa fa-newspaper-o"></i></span>
                                </div>
                                <div class='col-xs-9'>
                                    <strong>INDUSTRY NEWS & <br>DESIGNER PROFILES</strong>
                                </div>
                            </article>
                        </div>
                        <br>
                        <div class="row">
                        	<article class="benefit_block">
                            	<div class='col-xs-2 text-right'>
                                    <span><i class="fa fa-user"></i></span>
                                </div>
                                <div class='col-xs-9' style="max-width:290px">
                                    <strong>DEDICATED CONSULTANT TO SUPPORT YOUR BUSINESS</strong>
                                </div>
                            </article>
                        </div>
                        <br>
                        <div class="row">
                        	<article class="benefit_block">
                            	<div class='col-xs-2 text-right'>
                                    <span><i class="fa fa-bookmark"></i></span>
                                </div>
                                <div class='col-xs-9'>
                                    <strong>REGULAR UPDATES ON LAST MINUTE STOCK AVAILABILITY</strong>
                                </div>
                            </article>
                        </div>
                        <br>




	                </div>
	                <!--<div class="benefit_image pt-1em"><img src="<?php bloginfo('template_url'); ?>/img/memberbenefits.jpg" alt="Member Benefit"></div>-->
	            </div>
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>
