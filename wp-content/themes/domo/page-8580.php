<?php get_header(); 
// This is about domo page
?>

<div class="content pt-1em pb-3em">
    <div class="container">
        <div class="inner-full-width about-domo-page">
        	<div class="row clearfix">
            	<div class="col-md-6 domos_collection">
                	<header class="small_brown_txt">DOMO COLLECTIONS //</header>
                    <?php echo get_field('domo_collection_content'); ?>
                </div>
                <div class="col-md-6 domo_brand_img">
                	<?php $img = get_field('domo_collection_brand_image'); if($img): ?>
                		<figure><img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>"></figure>
                    <?php endif; ?>
                </div>
            </div>
            
            
            <div class="about_two_blocks">
                <div class="row clearfix">
                	<div class="col-sm-6 mmb-2em">
                    	<?php echo get_field('about_left_block'); ?>
					</div>
                    <div class="col-sm-6">
                    	<?php echo get_field('about_right_block'); ?>
					</div>
                </div>
            </div>
            
            
            <div class="row clearfix founder-info">
            	<div class="col-md-6 founder-detail mmb-2em">
                	<header class="small_brown_txt">MEET THE FOUNDER //</header>
                    <h4><?php echo get_field('founder_name'); ?></h4>
                    <?php echo get_field('founder_detail_info'); ?>
                </div>
            	<div class="col-md-6 domo_brand_img founder_img">
                	<?php $img = get_field('founder_image'); if($img): ?>
                	<figure><img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>"></figure> <?php endif; ?>
                    <div class="founder-quote pt-3em">
                    	<div><img src="<?php bloginfo('template_url'); ?>/img/quote.svg" alt="quote" width="63"></div>
                    	<header><?php echo get_field('founder_quote'); ?></header>
                        <div class="pb-2em"><img src="<?php bloginfo('template_url'); ?>/img/quote-right.svg" alt="quote" width="63"></div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<?php get_footer(); ?>
