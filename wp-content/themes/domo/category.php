<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _domo
 */

get_header(); ?>
<div class="content ptb-1em">
	<div class="container">
    	<div class="row clearfix">
        	<div class="single-left col-sm-8 col-md-9 mmb-2em">
            	<div class="news-archives">
                
                
                		<?php while ( have_posts() ) : the_post(); ?>
                        <?php $thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true); 
							$thumb_url = $thumb_url_array[0]; ?>

							<article class="clearfix">
                                    <figure style="background-image: url('<?php echo $thumb_url; ?>');"></figure>
                                    <div class="blog-minicontent">
                                        <header>
                                            <h4><a href="<?php the_permalink(); ?>"><?php $post_title = get_the_title(); echo substr($post_title, 0, 60); ?> </a></h4>
                                            <div class="posted_date_month"><span class="current_post_date"><?php $date_store = date("d", strtotime($post->post_date)); echo ltrim($date_store, 0); ?></span><span class="current_post_month text-uppercase"><?php echo date("M", strtotime($post->post_date)); ?></span></div>
                                        </header>
                                        <div class="post-meta"><small>POSTED BY <?php echo strtoupper(get_the_author()); ?></small></div>
                                        <div class="related_post_cat pb-1em">
                                            <?php  $category = get_the_category();
                                            foreach ($category as $outpuvalue){
                                                echo "<span><a href='" . get_category_link( $outpuvalue->term_id ) . "'>" . $outpuvalue->cat_name . "</a></span>";
                                            }
                                            ?>
                                        </div>
                                        <p><?php $excerpt = get_the_content(); if (strlen($excerpt) > 255) { $excerpt = substr($excerpt, 0, 255) . "..."; echo $excerpt; } ?></p>
                                        <a href="<?php the_permalink(); ?>" class="read_more_btn_outline">read</a>
                                    </div>
                            </article>
            
                        <?php endwhile; ?>
            
                        <!-- Pagination Nav -->
                        <div class="pagination-nav">
                             <?php the_posts_pagination( $args ); ?> 
                        </div>
                        <!-- End Pagination Nav -->
                    
                    	
                </div>
                
                
                
                
            </div>
            <?php get_sidebar(); ?>
        </div>
    	
    </div>
</div>

<?php get_footer(); ?>
