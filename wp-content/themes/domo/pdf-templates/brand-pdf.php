<?php
include "../../../../wp-load.php";
require_once '../includes/dompdf/autoload.inc.php';
require_once '../includes/Mustache/Autoloader.php';
Mustache_Autoloader::register();
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

// instantiate and use the dompdf class

$dompdf = new Dompdf();
$dompdf->set_option('enable_remote', TRUE);
$dompdf->set_option('enable_css_float', TRUE);

$m = new Mustache_Engine;

if (!isset($_GET['brand'])){
  Print "No brand specified";
  return;
}

$brandID = $_GET['brand'];

$brand = get_post($brandID);

$image = get_field("lifestyle_image", $brandID)['sizes']['large'];

$html = $m->render(file_get_contents("brand.html"), array(
  'date' => date("jS F Y"),
  'name' => strtoupper($brand->post_title),
  'lifestyle_image' => $image,
  'country' => strtoupper(get_field("country", $brandID)),
  'content' => apply_filters('the_content', $brand->post_content)
  )
);

$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream($brand->post_name);
?>
