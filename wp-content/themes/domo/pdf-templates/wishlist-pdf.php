<?php
include "../../../../wp-load.php";
require_once '../includes/dompdf/autoload.inc.php';
require_once '../includes/Mustache/Autoloader.php';
Mustache_Autoloader::register();
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

// instantiate and use the dompdf class

$dompdf = new Dompdf();
$dompdf->set_option('enable_remote', TRUE);
$dompdf->set_option('enable_css_float', TRUE);

$m = new Mustache_Engine;

if (!isset($_GET['wishlist'])){
  Print "No wishlist specified";
  return;
}

$wishlistID = $_GET['wishlist'];

$options = array("is_default" => false, "wishlist_id" =>  $wishlistID);
if ($wishlistID == -1){
  $options = array("is_default" => true);
}
$products = YITH_WCWL()->get_products($options);

$wishlist = YITH_WCWL()->get_wishlists($options)[0];

$wishlist_name = $wishlist['wishlist_name'];

if ($wishlistID == -1){
  $wishlist_name = "My Wishlist";
}

for($i = 0; $i < count($products); $i++){
  $pf = new WC_Product_Factory();
  $show_line = true;
  $product = $pf->get_product($products[$i]['ID']);
  $products[$i]['image'] = wp_get_attachment_image_src(get_post_thumbnail_id($products[$i]['ID']), 'shop_single')[0];
  $products[$i]['post_title'] = strtoupper($products[$i]['post_title']);
  $brand_terms = get_the_terms($products[$i]['ID'], "brand");
  $brands = array();
  foreach($brand_terms as $term){
    array_push($brands, $term->name);
  }
  $products[$i]['brand'] = strtoupper($brands[0]);

  $price = get_field("display_price", $products[$i]['ID']);
  $online_purchase = get_field("available_for_online_purchase", $products[$i]['ID']);

  $stock_status = "Contact us";

  if ($online_purchase){
    $price = $product->get_price_html();
    if ($product->is_in_stock()){
      $stock_status = "In Stock";
    } else {
      $stock_status = "Out of Stock";
    }
  }

  if (($i + 1) % 5 === 0 || $i == count($products) - 1){
    $show_line = false;
  }
  $products[$i]['price'] = $price;
  $products[$i]['stock_status'] = $stock_status;
  $products[$i]['show_line'] = $show_line;
  $products[$i]['sku'] = $product->get_sku();
}

$html = $m->render(file_get_contents("wishlist.html"), array(
  'date' => date("jS F Y"),
  'name' => $wishlist_name,
  'products' => $products
  )
);

$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream("wishlist");
?>
