<?php
include "../../../../wp-load.php";
require_once '../includes/dompdf/autoload.inc.php';
require_once '../includes/Mustache/Autoloader.php';
Mustache_Autoloader::register();
// reference the Dompdf namespace
use Dompdf\Dompdf;
use Dompdf\Options;

// instantiate and use the dompdf class

$dompdf = new Dompdf();
$dompdf->set_option('enable_remote', TRUE);
$dompdf->set_option('enable_css_float', TRUE);

$m = new Mustache_Engine;

if (!isset($_GET['product'])){
  Print "No product ID specified";
  return;
}

$productID = $_GET['product'];

$pf = new WC_Product_Factory();
$product = $pf->get_product($productID);
//print_r($product);

$brand_terms = get_the_terms($productID, "brand");
$brands = array();
foreach($brand_terms as $term){
  array_push($brands, $term->name);
}

$purchase_description_text = "IN-STORE PURCHASE ONLY";
$price = get_field("display_price", $productID);
$online_purchase = get_field("available_for_online_purchase", $productID);

if ($online_purchase){
  $purchase_description_text = "AVAILABLE FOR ONLINE PURCHASE - PICK UP AT YOUR LOCAL STORE ONLY";
  $price = $product->get_price_html();
}

$variations = get_field("colour_upholstery_variations", $productID);
$variation_images = array();
foreach($variations as $variation){
  array_push($variation_images, array("src" => $variation['sizes']['thumbnail']));
}

$dimensions = get_field("dimensions", $productID);
$images = array();
$attachment_ids = $product->get_gallery_attachment_ids();

foreach($attachment_ids as $attachment){
  if (count($images) == 2){
    break;
  }
  array_push($images, array("src" => wp_get_attachment_image_src($attachment, 'shop_single')[0]));
}


$html = $m->render(file_get_contents("product.html"), array(
  'date' => date("jS F Y"),
  'name' => strtoupper($product->post->post_title),
  'brand' => strtoupper($brands[0]),
  'purchase_text' => $purchase_description_text,
  'price' => $price,
  'content' => $product->post->post_content . "<br><br>" . get_field("additional_information", $productID),
  'dimensions' => $dimensions,
  'variations' => $variation_images,
  'images' => $images,
  'image' => wp_get_attachment_image_src(get_post_thumbnail_id($productID), 'shop_single')[0]
  )
);

$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portrait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream($product->post->post_name);
?>
