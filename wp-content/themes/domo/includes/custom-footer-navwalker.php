<?php
class Nav_Custom_Footer_Walker extends Walker_Nav_Menu {
public $level_count = 0;
  // add classes to ul sub-menus
function start_lvl( &$output, $depth ) {
    // depth dependent classes
    $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
    $display_depth = ( $depth + 1); // because it counts the first submenu as 0
    if ($depth == 0){
      $output .= "<ul>";
    }
}

function end_lvl( &$output, $depth ) {
  if ($depth == 0){
    $output .= "</ul>";
  }
}

// add main/sub classes to li's and links
 function start_el( &$output, $item, $depth, $args ) {
    global $wp_query;

    if ($depth == 0){
      $output .= "\n" . $indent . "<div class='col-sm-3 mmb-3em'><ul class='footer-dropdown-list'>";
    }
    $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
    // passed classes
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

    // build html
    $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

    $linkhref = $item->url;

    $title = apply_filters( 'the_title', $item->title, $item->ID );

    if ($depth == 0){
      $item_output = '<h4>' . $title . '</h4>';
    }

    if ($depth == 1){
      $item_output = "<a href='" . $linkhref . "'>" . $title . "</a>";
    }

    // build html
    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
}

function end_el( &$output, $object, $depth, $args) {
  if ($depth == 0){
    $output .= "</ul></div>";
  }
}


}

?>
