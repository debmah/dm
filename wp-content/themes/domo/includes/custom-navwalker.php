<?php
class Nav_Custom_Walker extends Walker_Nav_Menu {
public $level_count = 0;
public $dropdown_height = 7;
  // add classes to ul sub-menus
function start_lvl( &$output, $depth ) {
    // depth dependent classes
    $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
    $display_depth = ( $depth + 1); // because it counts the first submenu as 0
    $classes = array(
        'sub-menu',
        ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
        ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
        'menu-depth-' . $display_depth
        );
    $class_names = implode( ' ', $classes );
    if ($display_depth == 1){
      $this->level_count = 0;
      $output .= "<div class='dropdown-menu clearfix' style='display: none;'><div class='container'>";
    }
    // build html
    $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
}

function end_lvl( &$output, $depth ) {
  $output .= "</ul>";
  if ($depth == 1){
    $output .= "</div></div></div>";
  }
}

// add main/sub classes to li's and links
 function start_el( &$output, $item, $depth, $args ) {
    if ($depth == 0){
      $dropdown = $item->post_name;
      $this->dropdown_height = 5;
      if ($dropdown == "brand-profiles"){
        $this->dropdown_height = 5;
      }
      if ($dropdown == "company-2"){
        $this->dropdown_height = 3;
      }
      if ($dropdown == "products-2"){
        $this->dropdown_height = 5;
      }
    }
    $this->level_count++;
    $type = $item->object;
    global $wp_query;
    $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent

    if (($this->level_count % $this->dropdown_height) == 1 && $depth == 1){
      if ($this->level_count != 1){
        $output .="</div>";
      }
      if ($this->level_count == 1){
        $output .="<div class='menu-col first_col'>";
      } else {
        $output .="<div class='menu-col'>";
      }
    }

    if (($this->level_count % $this->dropdown_height) != 1 && $depth == 1 && $type == "menu_image"){
      $output .="</div>";
      $output .="<div class='menu-col'>";
    }

    // depth dependent classes
    $depth_classes = array(
        ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
        ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
        ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
        'menu-item-depth-' . $depth
    );
    $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );

    // passed classes
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    if ($args->walker->has_children){
      array_push($classes, "dropdown");
    }
    $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );

    // build html
    $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';

    $dropdownStatus = "";
    if ($args->walker->has_children){
      $dropdownStatus = "&nbsp;<i class='fa fa-angle-down' aria-hidden='true'></i>";
    }

    $linkhref = $item->url;

    if ($type == "menu_image"){
      //$linkhref = get_field("linked_page", $item->object_id);
    }

    // link attributes
    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $linkhref )        ? ' href="'   . esc_attr( $linkhref        ) .'"' : '';
    if ($args->walker->has_children){
      $attributes .= 'class="dropdown-toggle" data-toggle="dropdown"';
    }

    if ($type == "menu_image"){
      $attributes .= 'class="menu-image"';
    }

    $title = apply_filters( 'the_title', $item->title, $item->ID );

    if ($type == "menu_image"){
      $image = get_field("image", $item->object_id)['sizes']['medium'];
      $title = "<div class='menu-image-holder' style='background-image:url(\"" . $image . "\");'></div>";
      $this->level_count+= ($this->dropdown_height - ($this->level_count % $this->dropdown_height));
    }

    if ($item->object == "brand_profile"){
      $logoimg = get_field("logo", $item->object_id)['sizes']['medium'];
      if ($logoimg != null){
        $attributes .= ' data-hover-image="' . $logoimg . '"';
      }
    };

    if ($type != "menu_image"){
      $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s%6$s</a>%7$s',
      $args->before,
      $attributes,
      $args->link_before,
      $title,
      $dropdownStatus,
      $args->link_after,
      $args->after
    );
  } else {
    $item_output = sprintf( '%1$s%3$s%4$s%5$s%6$s%7$s',
    $args->before,
    $attributes,
    $args->link_before,
    $title,
    $dropdownStatus,
    $args->link_after,
    $args->after
  );
  }

    // build html
    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
}
}

?>
