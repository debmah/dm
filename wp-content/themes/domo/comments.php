<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to _domo_comment() which is
 * located in the includes/template-tags.php file.
 *
 * @package _domo
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() )
	return;
?>

<div id="comments" class="comments-area">
  <?php // You can start editing here -- including this comment! ?>
  <?php if ( have_comments() ) : ?>
  <header>
    <h2 class="comments-title">
      <?php
					printf( _nx( '1 Comment', '%1$s Comments', get_comments_number(), '', '_domo' ),
						number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
				?>
    </h2>
  </header>
  <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
  <nav id="comment-nav-above" class="comment-navigation" role="navigation">
    <h5 class="screen-reader-text">
      <?php _e( 'Comment navigation', '_domo' ); ?>
    </h5>
    <ul class="pager">
      <li class="nav-previous">
        <?php previous_comments_link( __( '&larr; Older Comments', '_domo' ) ); ?>
      </li>
      <li class="nav-next">
        <?php next_comments_link( __( 'Newer Comments &rarr;', '_domo' ) ); ?>
      </li>
    </ul>
  </nav>
  <!-- #comment-nav-above -->
  <?php endif; // check for comment navigation ?>
  <ol class="comment-list media-list">
    <?php
				/* Loop through and list the comments. Tell wp_list_comments()
				 * to use _domo_comment() to format the comments.
				 * If you want to overload this in a child theme then you can
				 * define _domo_comment() and that will be used instead.
				 * See _domo_comment() in includes/template-tags.php for more.
				 */
				wp_list_comments( array( 'callback' => '_domo_comment', 'avatar_size' => 50 ) );
			?>
  </ol>
  <!-- .comment-list -->

  <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
  <nav id="comment-nav-below" class="comment-navigation" role="navigation">
    <h1 class="screen-reader-text">
      <?php _e( 'Comment navigation', '_domo' ); ?>
    </h1>
    <div class="nav-previous">
      <?php previous_comments_link( __( '&larr; Older Comments', '_domo' ) ); ?>
    </div>
    <div class="nav-next">
      <?php next_comments_link( __( 'Newer Comments &rarr;', '_domo' ) ); ?>
    </div>
  </nav>
  <!-- #comment-nav-below -->
  <?php endif; // check for comment navigation ?>
  <?php endif; // have_comments() ?>
  <?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?>
  <p class="no-comments">
    <?php _e( 'Comments are closed.', '_domo' ); ?>
  </p>
  <?php endif; ?>
  <?php comment_form( $args = array(
			  'id_form'           => 'commentform',  // that's the wordpress default value! delete it or edit it ;)
			  'id_submit'         => 'commentsubmit',
			  'title_reply'       => __( 'Log in to Leave a Comment', '_domo' ),  // that's the wordpress default value! delete it or edit it ;)
			  'title_reply_to'    => __( 'Leave a Comment to %s', '_domo' ),  // that's the wordpress default value! delete it or edit it ;)
			  'cancel_reply_link' => __( 'Cancel Comment', '_domo' ),  // that's the wordpress default value! delete it or edit it ;)
			  'label_submit'      => __( 'Post Comment', '_domo' ),  // that's the wordpress default value! delete it or edit it ;)

			  'fields' => apply_filters( 'comment_form_default_fields', array(
					'author' => '<p class="comment-form-author">' .  '<input id="author" name="author" type="text" placeholder="Name *" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p>',
					'email'  => '<p class="comment-form-email">' . '<input id="email" name="email" placeholder="Email*" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' />'.'</p>',
					'url'    => '',
					 ) ),

			  'comment_field' =>  '<p><textarea placeholder="Your Comment..." id="comment" class="form-control" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>',

			  'must_log_in' => '<p class="must-log-in">' .
				sprintf(
				  __( ' <a href="%s">Log in</a> to leave a comment.' ),
				  wp_login_url( apply_filters( 'the_permalink', get_permalink() ) )
				) . '</p>',

				'comment_notes_before' => '<p class="comment-notes">' .
				__( 'Your email address will not be published, all comments are approved before posted' ) . ( $req ? $required_text : '' ) .
				'</p>',

			  /*'comment_notes_after' => '<p class="form-allowed-tags">' .
				__( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes:', '_domo' ) .
				'</p><div class="alert alert-info">' . allowed_tags() . '</div>'*/

			  // So, that was the needed stuff to have bootstrap basic styles for the form elements and buttons

			  // Basically you can edit everything here!
			  // Checkout the docs for more: http://codex.wordpress.org/Function_Reference/comment_form
			  // Another note: some classes are added in the bootstrap-wp.js - ckeck from line 1

	));

	?>
</div>
<!-- #comments -->
