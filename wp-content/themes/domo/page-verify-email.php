<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package _domo
 */

get_header(); ?>
<?php
/* Check whether we need to verify a user */
if (isset($_GET["verify"]) && isset($_GET["user"]) && isset($_GET["code"])) {
	$code = $_GET["code"];
	$user = (int)$_GET["user"];
	/* Verify the user */
	update_user_meta($user, "email_verified", "true");
	/* Get the user type */
	$type = get_user_meta($user, "user_type", true);
	/* Get the user data */
	$data = unserialize(get_user_meta($user, "user_data", true));
	/* Send welcome email */
	send_welcome_email($data["firstName"], $data["email"], $type, $data);
}
?>
<div class="content ptb-2em">
<div class="container">
	<div class="inner-full-width">
			<br /><br />
	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' ); ?>

		<?php
			// If comments are open or we have at least one comment, load up the comment template
			if ( comments_open() || '0' != get_comments_number() )
				comments_template();
		?>

	<?php endwhile; // end of the loop. ?>
	<br /><br />
    </div>
</div>
</div>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
