<?php
get_header();
/* Template Name: Stockist*/
?>
<div class="content pb-2em">
  <div class="container">
    <div class='row'>
      <div class='col-sm-12'>
        <p>
          DOMO Stockists are currently trading in the below locations:
        </p>
        <br>
        <?php
        $terms = get_terms( array(
          'taxonomy' => 'state',
          'hide_empty' => true,
        ) );

        $states = array();
        $states['VIC'] = false;
        $states['NSW'] = false;
        $states['QLD'] = false;
        $states['TAS'] = false;
        $states['SA'] = false;
        $states['WA'] = false;
        $states['ACT'] = false;
        $states['NT'] = false;

        foreach ($terms as $term){
          $state = $term->name;
          if ($state == "Victoria"){
            $states['VIC'] = true;
          }
          if ($state == "New South Wales"){
            $states['NSW'] = true;
          }
          if ($state == "Queensland"){
            $states['QLD'] = true;
          }
          if ($state == "Tasmania"){
            $states['TAS'] = true;
          }
          if ($state == "South Australia"){
            $states['SA'] = true;
          }
          if ($state == "Western Australia"){
            $states['WA'] = true;
          }
          if ($state == "Australian Capital Territory"){
            $states['ACT'] = true;
          }
          if ($state == "Northern Territory"){
            $states['NT'] = true;
          }
        }

        if ($states['VIC']){
          ?>
          <a href='<?php echo get_site_url();?>/stores/?state=victoria'><button class='brown-outline-btn state-btn'>VIC <i class='fa fa-chevron-right'></i></button></a>
          <?php
        }

        if ($states['NSW']){
          ?>
          <a href='<?php echo get_site_url();?>/stores/?state=new-south-wales'><button class='brown-outline-btn state-btn'>NSW <i class='fa fa-chevron-right'></i></button></a>
          <?php
        }

        if ($states['SA']){
          ?>
          <a href='<?php echo get_site_url();?>/stores/?state=south-australia'><button class='brown-outline-btn state-btn'>SA <i class='fa fa-chevron-right'></i></button></a>
          <?php
        }

        if ($states['QLD']){
          ?>
          <a href='<?php echo get_site_url();?>/stores/?state=queensland'><button class='brown-outline-btn state-btn'>QLD <i class='fa fa-chevron-right'></i></button></a>
          <?php
        }

        if ($states['TAS']){
          ?>
          <a href='<?php echo get_site_url();?>/stores/?state=tasmania'><button class='brown-outline-btn state-btn'>TAS <i class='fa fa-chevron-right'></i></button></a>
          <?php
        }

        if ($states['WA']){
          ?>
          <a href='<?php echo get_site_url();?>/stores/?state=western-australia'><button class='brown-outline-btn state-btn'>WA <i class='fa fa-chevron-right'></i></button></a>
          <?php
        }

        if ($states['ACT']){
          ?>
          <a href='?state=australian-capital-territory'><button class='brown-outline-btn state-btn'>ACT <i class='fa fa-chevron-right'></i></button></a>
          <?php
        }

        if ($states['NT']){
          ?>
          <a href='?state=northern-territory'><button class='brown-outline-btn state-btn'>NT <i class='fa fa-chevron-right'></i></button></a>
          <?php
        }
        ?>
        <a href='<?php echo get_permalink(21165); ?>'><button class='brown-outline-btn state-btn'>STOCKISTS <i class='fa fa-chevron-right'></i></button></a>
      </div>
    </div>
    <br>


  <?php $args = array('post_type' => 'domo_stockist'); ?>
  <?php $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) :
    while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
    <div class='store-location-card'>
      <div class='row'>
        <div class='col-sm-6'>
          <div class='row'>
            <div class='col-sm-12'>
              <div class='row'>
                <div class='col-sm-12'>
                  <h2> <?php Print strtoupper(get_the_title()); ?> </h2>
                  <p class='store-state'><?php Print get_field("state"); ?></p>
                  <p class='phone-number'>
                    <a href='tel:<?php Print get_field("phone_number"); ?>'><i class='icon fa fa-phone'></i><?php Print get_field("phone_number"); ?></a>
                  </p>
                </div>
              </div>

              <div class='row'>
                <div class='col-sm-6'>
                  <i class="icon fa fa-map-marker" aria-hidden="true"></i> <?php Print get_field("address"); ?>
                  <br>
                </div>
                <div class='col-sm-6'>
                  <?php
                  if (get_field("parking_information") != ""){
                    ?>
                    <p>
                      <i class="icon fa fa-car" aria-hidden="true"></i> <small>Parking Info</small>
                      <br>
                      <?php Print get_field("parking_information"); ?>
                    </p>
                    <?php
                  }

                  ?>
                  <br>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <p>
                    <i class="icon fa fa-clock-o" aria-hidden="true"></i>
                    <small>OPENING HOURS</small>
                  </p>
                  <p class='indent'>
                    <?php
                    $days = get_field("opening_hours");
                    foreach ($days as $day){
                      Print "<small>" . $day['days'] . "</small><br>";
                      Print $day['hours'] . "<br><br>";
                    }
                    ?>
                  </p>
                </div>
                <div class="col-sm-6">
                  <?php
                  if (get_field("fax_number") != ""){
                    ?>
                    <p>
                      <i class="icon fa fa-fax" aria-hidden="true"></i> <small>Fax</small>
                      &nbsp; <?php Print get_field("fax_number"); ?>
                    </p>
                    <br>
                    <?php
                  }


                  if (get_field("head_office") == true){
                    ?>
                    <p>
                      <i class="icon fa fa-certificate" aria-hidden="true"></i> <small>HQ</small>
                      &nbsp; DOMO Head Office
                    </p>
                    <?php
                  }

                  if (get_field("warehouse") == true){
                    ?>
                    <p>
                      <i class="icon fa fa-truck" aria-hidden="true"></i>
                      &nbsp; DOMO Warehouse
                    </p>
                    <?php
                  }


                  if (get_field("additional_information") != ""){
                    ?>
                    <br>
                    <?php Print get_field("additional_information"); ?>
                    <?php
                  }
                  ?>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <a href='<?php Print get_permalink(); ?>' class='btn'>VIEW STORE PROFILE</a>
                </div>
                <div class="col-md-6">
                  <a target="_blank" href='https://maps.google.com?daddr=<?php Print get_field("latitude"); ?>,<?php Print get_field("longitude");?>' class='btn btn-brown'>GET DIRECTIONS</a>
                </div>
              </div>
              <br>
            </div>
          </div>
        </div>
        <div class='col-sm-6'>
          <div class='stockist_map' data-lat="<?php Print get_field('latitude'); ?>" data-lng=<?php Print get_field('longitude'); ?>></div>
        </div>
      </div>
    </div>
  <?php endwhile; endif; wp_reset_postdata(); ?>

</div>
</div>
<?php get_footer(); ?>
