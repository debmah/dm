<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>

<?php
$brand_terms = get_the_terms($product->ID, "brand");
$brands = array();
foreach((array)$brand_terms as $term){
	array_push($brands, $term->slug);
}

$style_terms = get_the_terms($product->ID, "style");
$styles = array();
foreach((array)$style_terms as $term){
	array_push($styles, $term->slug);
}

$cat_terms = get_the_terms($product->ID, "product_cat");
$cats = array();
foreach((array)$cat_terms as $term){
	array_push($cats, $term->slug);
}

$type_terms = get_the_terms($product->ID, "domo_product_type");
$types = array();
foreach((array)$type_terms as $term){
	array_push($types, $term->slug);
}

$upholstery_terms = get_the_terms($product->ID, "upholstery");
$upholstery = array();
foreach((array)$upholstery_terms as $term){
	array_push($upholstery, $term->slug);
}

?>

<div class='col-sm-3 product-filter-item' data-brand="<?php Print implode(" ", $brands); ?>" data-style="<?php Print implode(" ", $styles); ?>" data-product-cat="<?php Print implode(" ", $cats); ?>" data-domo-product-type="<?php Print implode(" ", $types); ?>" data-upholstery="<?php Print implode(" ", $upholstery); ?>">
	<a href='<?php Print get_the_permalink(); ?>'>
		<div class='domo-product'>
			<?php Print woocommerce_get_product_thumbnail(); ?>
			<span class='product-title'><?php the_title(); ?></span>
			<?php
			$brand = "";
			$brand_terms = get_the_terms($product->ID, 'brand');
			if (count($brand_terms) > 0){
				$brand = $brand_terms[0]->name;
			}
			?>
			<span class='product-brand'><?php Print $brand; ?></span>
		</div>
	</a>
</div>
