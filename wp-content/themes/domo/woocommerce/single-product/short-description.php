<?php
/**
* Single product short description
*
* This template can be overridden by copying it to yourtheme/woocommerce/single-product/short-description.php.
*
* HOWEVER, on occasion WooCommerce will need to update template files and you
* (the theme developer) will need to copy the new files to your theme to
* maintain compatibility. We try to do this as little as possible, but it does
* happen. When this occurs the version of the template file will be bumped and
* the readme will list any important changes.
*
* @see 	    https://docs.woocommerce.com/document/template-structure/
* @author 		WooThemes
* @package 	WooCommerce/Templates
* @version     1.6.4
*/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

if ( ! $post->post_content ) {
	return;
}

?>
<div itemprop="description">
	<?php the_content(); ?>
	<?php Print get_field("additional_information"); ?>
	<?php
	$online_purchase = get_field("available_for_online_purchase");
	if ($online_purchase){
		Print "<p>This product is available for online purchase, with pickup only at your local DOMO store.<br>Contact us for further details or to arrange special pickup or delivery.</p>";
	}

	if (get_field("dimensions") != ""){
		?>
		<b>Dimensions</b>
		<br><br>
		<?php Print get_field("dimensions"); ?>
		<br><br>
		<?php
	}

	if (get_field("colour_upholstery_variations") != ""){
		?>
		<b>Colour / Upholstery Variations</b>
		<br><br>
		<?php $variations = get_field("colour_upholstery_variations"); ?>
		<?php
		foreach($variations as $variation){
			Print "<img class='variation-img' src='" . $variation['sizes']['thumbnail'] . "' width='60px' />";
		}
		?>
		<br><br>

		<?php
	}
	?>
</div>
