<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.2
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

global $post, $product;
?>
<div class="images single-img">
	<?php
	//Get all the promotions
	$promotions = new WP_Query(array('post_type' => 'promotion', 'posts_per_page' => -1, 'post_status' => 'publish'));
	$product_promotion = array();
	$promotion_link = "";
	if ( $promotions->have_posts() ){
	  while ( $promotions->have_posts() ) : $promotions->the_post();
	  $p = get_field("products");
	  foreach((array)$p as $promotion_product){
	    $products_in_promotion[$promotion_product->ID] = true;
			$product_promotion[$promotion_product->ID] = get_the_ID();
	  }
	  endwhile;
	}
	wp_reset_postdata();
	$is_on_sale = get_field("product_on_sale");
	$is_on_clearance = get_field("product_on_clearance");
	$is_on_promotion = $products_in_promotion[get_the_ID()];
	if ($is_on_promotion){
		// Get the promotion this product is associated with
		$promotion_link = get_permalink($product_promotion[get_the_ID()]);
	}
	$is_click_collect = get_field("available_for_online_purchase");
	?>
	<div class="product-tags">
		<?php if ($is_on_sale){?><span class="sale">sale</span><?php }?>
		<?php if ($is_on_promotion){?><a href="<?php Print $promotion_link; ?>"><span class="promotion">promotion</span></a><?php } ?>
		<?php if ($is_on_clearance){?><span class="clearance">clearance</span><?php } ?>
		<?php if ($is_click_collect){?><span class="click-and-collect">click & collect</span> <?php } ?>
	</div>
	<?php
		if ( has_post_thumbnail() ) {
			$attachment_count = count( $product->get_gallery_attachment_ids() );
			$gallery          = $attachment_count > 0 ? '[product-gallery]' : '';
			$props            = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
			$image            = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
				'title'	 => $props['title'],
				'alt'    => $props['alt'],
			) );
			echo apply_filters(
				'woocommerce_single_product_image_html',
				sprintf(
					'<a href="%s" itemprop="image" class="woocommerce-main-image zoom" title="%s" data-rel="prettyPhoto%s"><div class="single-product-full-image-holder">%s<div class="zoom_image"><i class="fa fa-search-plus" aria-hidden="true"></i></div></div></a>',
					esc_url( $props['url'] ),
					esc_attr( $props['caption'] ),
					$gallery,
					$image
				),
				$post->ID
			);
		} else {
			echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), __( 'Placeholder', 'woocommerce' ) ), $post->ID );
		}

		do_action( 'woocommerce_product_thumbnails' );
	?>

</div>
