<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

?>
<div itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="domo-offers">
  <?php
    $online_purchase = get_field("available_for_online_purchase");
    if ($online_purchase){
      Print "<header><strong>AVAILABLE FOR ONLINE PURCHASE - PICK UP AT YOUR LOCAL STORE ONLY</strong></header><br />";
			?>
			<p class="price"><?php echo $product->get_price_html(); ?></p>
			<meta itemprop="price" content="<?php echo esc_attr( $product->get_display_price() ); ?>" />
			<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
			<?php
    } ?>
    <?php $price = $product->get_display_price();
			if ($price == 0 || $price == ""){
				$price = get_field("display_price");
			} else {
				$price = $product->get_price_html();
			} ?>
    <p class="price"><?php echo $price; ?></p>
    <?php

	if(get_field('available_at_these_selected_domo_stores')){
	$post_objects = get_field('available_domo_stores'); }
	if( $post_objects ) { ?>
    	<header><strong>Available at these selected DOMO showrooms</strong></header>
        <ul class="domo-store-lists">
			<?php foreach( $post_objects as $post_object): ?>
                <li>
                    <a href="<?php echo get_permalink($post_object->ID); ?>" target="_blank"><?php echo get_the_title($post_object->ID); ?></a>
                </li>
            <?php endforeach; ?>
        </ul>
				<?php Print "<header class='pb-1em'><strong>Interstate freight charges may apply</strong></header>"; //this will show if there is any available domo store is active ?>

        <?php if (get_field("product_customisation_available")) {
				Print "<header><strong>Product customisation available</strong></header><br />";
			} ?>



	<?php }


	else {

		$clearance_product = get_field('product_on_clearance');
		if($clearance_product){
			Print "<header><strong>Please contact your local showroom for availability.<br class='hidden-xs' />Interstate freight charges may apply.</strong></header><br />";
		}
		else{

			$custom_order = get_field("custom_order_only");
			if ($custom_order){
				Print "<header><strong>Custom order only. Please enquire at a DOMO showroom near you</strong></header><br />";
			} else {
				Print "<header><strong>Please contact your local showroom for availability</strong></header><br />";
			}

			if (get_field('freight_charges_may_apply_limited_stock_available')){
				Print "<header><strong>Please contact your local showroom for availability.<br class='hidden-xs' />Interstate freight charges may apply.</strong></header><br />";
			}

		}



			if (get_field("product_customisation_available")) {
				Print "<header><strong>Product customisation available</strong></header><br />";
			}
		}









			?>

			<meta itemprop="price" content="<?php echo get_field("display_price"); ?>" />
			<meta itemprop="priceCurrency" content="<?php echo esc_attr( get_woocommerce_currency() ); ?>" />
			<?php

  ?>

</div>
