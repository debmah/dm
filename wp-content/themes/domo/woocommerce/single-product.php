<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
<div class="content ptb-2em">
<div class="container">
	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 */
		do_action( 'woocommerce_before_main_content' );
	?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php wc_get_template_part( 'content', 'single-product' ); ?>

		<?php endwhile; // end of the loop. ?>

	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

	<?php
		/**
		 * woocommerce_sidebar hook.
		 *
		 * @hooked woocommerce_get_sidebar - 10
		 */
		//do_action( 'woocommerce_sidebar' );
	?>
</div>
</div>

<div class="additional_information">
	<div class="additional_header"><div class="container"><h4>Additional information</h4></div></div>
    <div class="product-additional-info ptb-3em">
        <div class="container">
            <div class="row clearfix">
                <div class="col-sm-4 first-info">
                    <div class="information_download">
                        <p>Information PDF sheets and/or brochures available to download:</p>
                        <div class="download_buttons">
                            <span><a href="<?php Print get_stylesheet_directory_uri(); ?>/pdf-templates/product-pdf.php?product=<?php Print $post->ID; ?>" class="pdf-download"><i class="fa fa-file-text mr-8" aria-hidden="true"></i> PDF DOWNLOAD</a></span>
														<?php
														if (get_field("pdf_brochure")){
															?>
															<span><a href="<?php Print get_field("pdf_brochure"); ?>" class="pdf-download"><i class="fa fa-bookmark mr-8" aria-hidden="true"></i></i> PRODUCT BROCHURE</a></span>
															<?php
														}
															?>
                        </div>
                        <div class="shareit_online">
                            <strong>Do you love this product?</strong>
                            <span>Share it online</span>
														<div class="social_icons pt-1em">
																<a href="http://www.facebook.com/share.php?u=<?php print(urlencode(get_permalink())); ?>&title=<?php print(urlencode(the_title())); ?>" target="_blank" class="fb"><i class="fa fa-facebook"></i></a>
																<a href="http://twitter.com/intent/tweet?status=<?php print(urlencode(the_title())); ?>+<?php print(urlencode(get_permalink())); ?>" target="_blank" class="tt"><i class="fa fa-twitter"></i></a>
																<!-- <a href="" class="ig"><i class="fa fa-instagram"></i></a> -->
																<a href="http://pinterest.com/pin/create/bookmarklet/?media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' ); print(urlencode($url = $thumb['0']));?>
								&url=<?php print(urlencode(the_permalink())); ?>&is_video=false&description=<?php print(urlencode(get_the_title())); ?>" target="_blank" class="pt"><i class="fa fa-pinterest"></i></a>
																<a href="mailto:?subject=<?php print(urlencode(the_title())); ?>&body=Look at this...<?php print(urlencode(get_permalink())); ?>" class="el"> <i class="fa fa-envelope"></i> </a>
														</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 second-info">
                	<div class="enquire_this_product" id="myDiv">
                    	<small>ENQUIRE ABOUT THIS PRODUCT</small>
						<h3><?php if(is_archive()) { $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );  ?>
							<?php $enquired = $term->name; echo $enquired; ?> <?php }  else { ?>
                            <?php $enquired = get_the_title(); echo get_the_title(); ?><?php } ?></h3>
                        <div class="enquire-product-form">
														<?php
														global $post;
														$post_slug = $post->post_name;
														?>
                            <?php echo str_replace("%PRODUCT_URL%",get_site_url() . "/product/" . $post_slug, str_replace("%SUBJECT%",$enquired, do_shortcode('[contact-form-7 id="157" title="Product Enquiry"]'))); ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 third-info">
                	<div class="related-cat-brand-products">
                		<h5>QUICK CONNECT</h5>
										<small>CHOOSE YOUR NEAREST STORE</small>
										<div class='rounded-select-holder'>
											<form id='quick-connect-form'>
												<select class='form-control quick-connect-selector' value="" autocomplete="off">
													<option disabled selected value>Select State</option>
													<?php
													$states = get_terms( array(
														'taxonomy' => 'state',
														'hide_empty' => true,
													) );
													foreach($states as $state){
														?>
														<option value="<?php Print $state->slug; ?>"><?php Print $state->name; ?></option>
														<?php
													}
													?>
												</select>
											</form>
											<script>
											document.getElementById("quick-connect-form").reset();
											</script>
										</div>
                        <!-- <div class="related-products-showcase pt-1em"><?php echo do_shortcode('[related_products per_page="6"]'); ?></div> -->
										<hr>
										<div class='quick-connect-results'>
											<div class='results-none'>Select a state above to view store contact details</div>
											<?php
											foreach($states as $state){
												?>
												<div class='results-state-<?php Print $state->slug; ?>' style='display:none'>
													<?php
													//Get all the stores for this state
													$stores = get_posts(
													array(
														'posts_per_page' => -1,
														'post_type' => 'store',
														'tax_query' => array(
															array(
																'taxonomy' => 'state',
																'field' => 'state',
																'terms' => $state->term_id,
															)
														)
													)
												);
												foreach($stores as $store){
													$store_title = $store->post_title;
													$store_title = preg_replace('/\s+/', '', $store_title);
													Print "<span class='$store_title'>";
													Print "<b>DOMO " . $store->post_title . "</b><br>";
													Print "<a href='tel:" . get_field("phone_number", $store) . "'><span class='phone-number'><i class='fa fa-phone'></i> " . get_field("phone_number", $store) . "</span></a>";
													Print "<a href='" . get_permalink($store) . "' class='view-store-btn'>VIEW STORE DETAILS</a><hr />";
													Print "</span>";
												}
												?>
												</div>
												<?php
											}
											?>
										</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php get_footer( 'shop' ); ?>
