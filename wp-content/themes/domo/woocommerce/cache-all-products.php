<?php
error_reporting(0);
include "../../../../wp-load.php";

echo "Refreshing product cache.....<br>";
flush();

$products = array();
$latest_arrivals = array();
$sale_products = array();
$clearance_products = array();
$products_in_promotion = array();
$click_collect_products = array();
$args = array(
  'post_type' => 'product',
  'posts_per_page' => -1,
  'post_status' => 'publish',
  'orderby' => 'title',
  'order' => 'ASC'
);

//Get all the products
$loop = new WP_Query( $args );

//Get all the promotions
$promotions = new WP_Query(array('post_type' => 'promotion', 'posts_per_page' => -1, 'post_status' => 'publish'));

if ( $promotions->have_posts() ){
  while ( $promotions->have_posts() ) : $promotions->the_post();
  $p = get_field("products");
  foreach((array)$p as $promotion_product){
    $products_in_promotion[$promotion_product->ID] = true;
  }
  endwhile;
}
$_pf = new WC_Product_Factory();
$count = 0;
$total = $loop->post_count;
if ( $loop->have_posts() ) {
  while ( $loop->have_posts() ) : $loop->the_post();
  $product = array();
  $product['id'] = get_the_ID();
  $product['name'] = get_the_title();
  $product['link'] = get_the_permalink();
  $product['tags'] = array('sale' => get_field("product_on_sale"), 'clearance' => get_field("product_on_clearance"), 'promotion' => $products_in_promotion[get_the_ID()], 'clickcollect' => get_field("available_for_online_purchase"));
  $product['image'] = base64_encode(woocommerce_get_product_thumbnail());

  $brand_terms = get_the_terms($product['ID'], "brand");
  $brands = array();
  foreach((array)$brand_terms as $term){
    array_push($brands, $term->slug);
  }

  $style_terms = get_the_terms($product['ID'], "style");
  $styles = array();
  foreach((array)$style_terms as $term){
    array_push($styles, $term->slug);
  }

  $cat_terms = get_the_terms($product['ID'], "product_cat");
  $cats = array();
  foreach((array)$cat_terms as $term){
    array_push($cats, $term->slug);
  }

  $type_terms = get_the_terms($product['ID'], "domo_product_type");
  $types = array();
  foreach((array)$type_terms as $term){
    array_push($types, $term->slug);
  }

  $upholstery_terms = get_the_terms($product['ID'], "upholstery");
  $upholstery = array();
  foreach((array)$upholstery_terms as $term){
    array_push($upholstery, $term->slug);
  }

  $product['brand_name'] = $brand_terms[0]->name;
  $product['brand'] = implode(" ", $brands);
  $product['style'] = implode(" ", $styles);
  $product['product_cat'] = implode(" ", $cats);
  $product['domo_product_type'] = implode(" ", $types);
  $product['upholstery'] = implode(" ", $upholstery);

  array_push($products, $product);
  $wcproduct = $_pf->get_product(get_the_ID());
  if ($wcproduct->is_featured()){
    array_push($latest_arrivals, $product);
  }

  if (get_field("product_on_sale") == true){
    array_push($sale_products, $product);
  }

  if (get_field("product_on_clearance") == true){
    array_push($clearance_products, $product);
  }

  if (get_field("available_for_online_purchase") == true){
    array_push($click_collect_products, $product);
  }

  $count++;
  if ($count == ceil($total / 10)){
    Print "10% completed<br>";
  }
  if ($count == ceil($total / 5)){
    Print "20% completed<br>";
  }
  if ($count == ceil($total / 3.3)){
    Print "30% completed<br>";
  }
  if ($count == ceil($total / 2.5)){
    Print "40% completed<br>";
  }
  if ($count == ceil($total / 2)){
    Print "50% completed<br>";
  }
  if ($count == ceil($total / 1.67)){
    Print "60% completed<br>";
  }
  if ($count == ceil($total / 1.43)){
    Print "70% completed<br>";
  }
  if ($count == ceil($total / 1.25)){
    Print "80% completed<br>";
  }
  if ($count == ceil($total / 1.11)){
    Print "90% completed<br>";
  }
  flush();
endwhile;
}

set_transient( "products_cache", json_encode($products) );
set_transient( "latest_arrivals_products_cache", json_encode($latest_arrivals) );
set_transient( "clearance_products_cache", json_encode($clearance_products) );
set_transient( "sale_products_cache", json_encode($sale_products) );
set_transient( "click_collect_products_cache", json_encode($click_collect_products) );

echo "Done<br>";
echo "<a href='" . get_site_url() . "'>Return to site</a>";
flush();
?>
