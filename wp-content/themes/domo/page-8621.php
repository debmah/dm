<?php get_header();
// DOMO Warranty Page
?>
<div class="content ptb-2em">
<div class="container">

    <div class="domo-warranty blog-left-column">
			<?php
if( have_rows('domo_warranty') ):
	while ( have_rows('domo_warranty') ) : the_row(); ?>
    	<div class="row clearfix">
        	<div class="col-sm-8 col-md-8 col-lg-9 warranty-info">

                    	<article>
                            <h2><?php echo get_sub_field('warranty_title'); ?></h2>
                            <?php echo get_sub_field('warranty_description'); ?>
                        </article>

            </div>
            <div class="col-sm-4 col-md-4 col-lg-3 warranty_images">
            	<?php
								$img = get_sub_field('warranty_image');
							?>
                    	<figure><img src="<?php echo $img['url']; ?>" alt="<?php echo $img['alt']; ?>"></figure>

            </div>
        </div>
				<?php endwhile; endif; ?>


        <div class="domo-extra-advice">
        	<div class="row clearfix">
            	<div class="col-sm-7 col-md-8 extra_advise_content mmb-3em">
                    <?php echo get_field('domo_warranty_bottom_content'); ?>
                    <a href="<?php echo site_url(); ?>/stores/" class="view_more_btn">VIEW STORE LOCATIONS</a>
                </div>
                <div class="col-sm-5 col-md-4 become_member_blox">
                	<div class="memberbox">
                    	<div class="member-content-box text-center">
                        	<span class="become-a-member">BECOME A DOMO MEMBER & BE REWARDED</span>
                            <span class="clickto-findmore">CLICK TO FIND OUT MORE & SIGN UP</span>
                            <span><a href="<?php echo get_permalink(210); ?>" class="view_more_btn">find out more</a></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php get_footer(); ?>
