<?php get_header();
/* Template Name: Promotions */
?>
<div class="content ptb-1em">
	<div class="container">
    	<?php $img = get_field('hero_image', 8733); ?>
    	<div class="domo-promotion-banner" style="background-image: url(<?php echo $img['url']; ?>);">
        	<h1><?php echo get_field('hero_texts'); ?></h1>
        </div>


    	<div class="row clearfix">
        	<div class="single-left col-sm-12 col-md-12 mmb-2em">
            	<div class="news-archives">

                    	<?php
							$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
							$temp = $wp_query;
							$wp_query = null;
							$wp_query = new WP_Query();
							$wp_query -> query('post_type=promotion&showposts=6'.'&paged='.$paged);
							while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                            <?php $thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
							$thumb_url = $thumb_url_array[0]; ?>


                            <article class="clearfix">
                                    <figure style="background-image: url('<?php echo $thumb_url; ?>');"></figure>
                                    <div class="blog-minicontent">
                                        <header>
                                            <h4><a href="<?php the_permalink(); ?>"><?php $post_title = get_the_title(); echo substr($post_title, 0, 60); ?> </a></h4>
                                        </header>
                                        <p><?php $excerpt = get_the_content(); if (strlen($excerpt) > 255) { $excerpt = substr($excerpt, 0, 999) . "..."; echo wpautop($excerpt, true); } ?></p>
                                        <a href="<?php the_permalink(); ?>" class="read_more_btn_outline">find out more</a>
                                    </div>
                            </article>

                            <?php endwhile; ?>


                        <!-- Pagination Nav -->
                        <div class="pagination-nav">
                             <?php the_posts_pagination( $args ); ?>
                            <?php $wp_query = null; $wp_query = $temp; ?> <!-- this is to clear wp_query -->
                        </div>
                        <!-- End Pagination Nav -->
                </div>


            </div>
            <?php //get_sidebar(); ?>
        </div>
				<?php if ($cached_products == "[]" || $cached_products == null){
					?>
					<div class='no-products-found'>
						<b>There are currently no promotions</b>
						<p>Click below to view our full furniture range</p>
						<a href='<?php Print get_site_url(); ?>/shop' class='button'>DOMO FURNITURE RANGE</a>
					</div>
					<br /><br />
					<?php
				} ?>
    </div>
</div>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
