<?php get_header(); ?>
<div class="content ptb-2em">
<div class="container">
	<div class="inner-full-width contact-page">

    	<div class="row clearfix">
        	<div class="col-md-6">
            	<div class="contact_dtls">
                	<div class="contact_widget">
                        <h2>STORE LOCATIONS</h2>
                        <p>Click below to find a DOMO store near you.</p>
                        <a href="<?php echo site_url(); ?>/stores/" class="view_more_btn">VIEW STORE LOCATIONS</a>
                    </div>
                    <div class="contact_widget">
                    	<h2>HEAD OFFICE</h2>
                        <p><?php echo get_field('head_office_infotxt'); ?></p>
                        <?php
							$string = get_field('head_office_phone_number', 141);
							$res = preg_replace("/[^a-zA-Z0-9]/", "", $string);
						?>
                        <span class="contact-number"><a href="tel:<?php echo $res; ?>"><i class="fa fa-phone mr-8"></i><?php echo $string; ?></a></span>
                        <a href="mailto:<?php echo get_field('head_office_email_address'); ?>" class="view_more_btn">EMAIL HEAD OFFICE</a>
                    </div>
                    <div class="contact_widget">
                    	<h2>MEDIA ENQUIRIES</h2>
                        <p><?php echo get_field('media_enquiry_info_text'); ?></p>
                        <span class="contact-number"><a href="tel:<?php echo $res; ?>"><i class="fa fa-phone mr-8"></i><?php echo $string; ?></a></span>
                        <a href="mailto:<?php echo get_field('media_enquiry_email'); ?>" class="view_more_btn">MEDIA ENQUIRIES</a>
                    </div>
                    <div class="contact_widget">
                    	<header>Follow and like us online</header>
                        <div class="social-links pt-1em">
                        	<a href="<?php Print get_field("facebook_url", 141); ?>" target="_blank" class="facebook_url"><i class="fa fa-facebook"></i></a>
                            <a href="<?php Print get_field("instagram_url", 141); ?>" target="_blank" class="instagram_url"><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
            	<div class="contact_general_form quick_career_form">
                    <header>send us a message</header>
                    <p>Feel free to send us a message or provide us with feedback, we would love to hear from you.</p>
                    <?php echo do_shortcode('[contact-form-7 id="8653" title="General Contact Form for Contact Page"]'); ?>
                </div>
                <div class="membership_blox_contact">
                	<div class="membership_infoblox">
                        <div class="member-content-box text-center">
                            <span class="become-a-member">BECOME A DOMO MEMBER <br>& BE REWARDED</span>
                            <span class="clickto-findmore">CLICK TO FIND OUT MORE & SIGN UP</span>
                            <span><a href="<?php echo get_permalink(210); ?>" class="view_more_btn">find out more</a></span>
                        </div>
                    </div>
                    <div class="membership_infoblox_image">
                    	<img src="<?php bloginfo('template_url'); ?>/img/membership_img.jpg" alt="Join DOMO">
                    </div>
                </div>
            </div>
        </div>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php get_template_part( 'content', 'page' ); ?>

		<?php
			// If comments are open or we have at least one comment, load up the comment template
			if ( comments_open() || '0' != get_comments_number() )
				comments_template();
		?>

	<?php endwhile; // end of the loop. ?>
    </div>
</div>
</div>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
