<?php
 define('THEME_DIR_PATH', get_template_directory());
 define('THEME_DIR_URI', get_template_directory_uri());

include "includes/custom-navwalker.php";
include "includes/custom-footer-navwalker.php";

if ( ! isset( $content_width ) )
	$content_width = 750; /* pixels */

if ( ! function_exists( '_domo_setup' ) ) :
function _domo_setup() {
	global $cap, $content_width;
	add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );
	add_editor_style();
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	//add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );
	add_theme_support( 'custom-background', apply_filters( '_domo_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
		) ) );
	load_theme_textdomain( '_domo', THEME_DIR_PATH . '/languages' );
	register_nav_menus( array(
		'primary'  => __( 'Main Menu', '_domo' ),
		) );

}
endif; // _domo_setup
add_action( 'after_setup_theme', '_domo_setup' );

/* Register widgetized area and update sidebar with default widgets */
function _domo_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', '_domo' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		) );
}
add_action( 'widgets_init', '_domo_widgets_init' );

/* Enqueue scripts and styles */
function _domo_scripts() {
	wp_enqueue_style( '_domo-style', get_stylesheet_uri() );
	wp_enqueue_style( '_domo-mystyle', THEME_DIR_URI . '/css/style.css?time=' . filemtime(get_stylesheet_directory() . "/css/style.css") );
	wp_enqueue_script( '_domo-skip-link-focus-fix', THEME_DIR_URI . '/includes/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( '_domo-keyboard-image-navigation', THEME_DIR_URI . '/includes/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}

}
add_action( 'wp_enqueue_scripts', '_domo_scripts' );

require THEME_DIR_PATH . '/includes/custom-header.php';
require THEME_DIR_PATH . '/includes/template-tags.php';
require THEME_DIR_PATH . '/includes/extras.php';
require THEME_DIR_PATH . '/includes/bootstrap-wp-navwalker.php';
require THEME_DIR_PATH . '/includes/acf-tab.php';

/* woocommerce theme support*/
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
	add_theme_support( 'woocommerce' );
}


// Add a custom stylesheet to replace woocommerce.css
function use_woocommerce_custom_css() {
  wp_enqueue_style(
      'woocommerce-custom',
      get_template_directory_uri() . '/woocommerce/woocommerce.css'
  );
}
add_action('wp_enqueue_scripts', 'use_woocommerce_custom_css', 15);
function get_random_brand_profile($exclude){
  $posts = get_posts(array('orderby' => 'rand', 'numberposts' => 1, 'post_type' => 'brand_profile', 'exclude' => $exclude));
  if ($posts != NULL || count($posts) != 0){
    return $posts[0];
  }
}


function domo_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'News';
    $submenu['edit.php'][5][0] = 'News';
    $submenu['edit.php'][10][0] = 'Add News';
    $submenu['edit.php'][16][0] = 'News Tags';
}
function domo_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'News';
    $labels->singular_name = 'News';
    $labels->add_new = 'Add News';
    $labels->add_new_item = 'Add News';
    $labels->edit_item = 'Edit News';
    $labels->new_item = 'News';
    $labels->view_item = 'View News';
    $labels->search_items = 'Search News';
    $labels->not_found = 'No News found';
    $labels->not_found_in_trash = 'No News found in Trash';
    $labels->all_items = 'All News';
    $labels->menu_name = 'News';
    $labels->name_admin_bar = 'News';
}

add_action( 'admin_menu', 'domo_change_post_label' );
add_action( 'init', 'domo_change_post_object' );

function wpdocs_custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

/*get popular posts*/
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

function register_my_menu() {
  register_nav_menu('footer',__( 'Footer Menu' ));
}
add_action( 'init', 'register_my_menu' );

function brand_init() {
	// create a new taxonomy
	register_taxonomy(
		'brand',
		'product',
		array(
			'hierarchical' => true,
			'label' => __( 'Brand' ),
			'rewrite' => array( 'slug' => 'brand' ),

      'show_admin_column' => true
		)
	);
}
add_action( 'init', 'brand_init' );

function upholstery_init() {
	// create a new taxonomy
	register_taxonomy(
		'upholstery',
		'product',
		array(
			'hierarchical' => true,
			'label' => __( 'Upholstery' ),
			'rewrite' => array( 'slug' => 'upholstery' ),
      'show_admin_column' => false
		)
	);
}
add_action( 'init', 'upholstery_init' );

function product_init() {
	// create a new taxonomy
	register_taxonomy(
		'domo_product_type',
		'product',
		array(
			'hierarchical' => true,
			'label' => __( 'Product Type' ),
			'rewrite' => array( 'slug' => 'product-type' ),
      'show_admin_column' => true
		)
	);
}
add_action( 'init', 'product_init' );

function style_init() {
	// create a new taxonomy
	register_taxonomy(
		'style',
		'product',
		array(
			'hierarchical' => true,
			'label' => __( 'Style' ),
			'rewrite' => array( 'slug' => 'style' ),
      'show_admin_column' => false
		)
	);
}
add_action( 'init', 'style_init' );

//add image size
add_image_size('blog_thumb', 165, 130, true);

//**Custom Gravatar**//
/*add_filter( 'avatar_defaults', 'bourncreative_custom_gravatar' );
function bourncreative_custom_gravatar ($avatar_defaults) {
$myavatar = get_stylesheet_directory_uri() . '/avatar.jpg';
$avatar_defaults[$myavatar] = "Custom Gravatar";
return $avatar_defaults;
} */

function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/* Hook the rebah_custom_excerpt_more() function to 'get_the_excerpt' filter hook */
add_filter( 'get_the_excerpt', 'rebah_custom_excerpt_more' );
function rebah_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= '...';
	}
	return $output;
}

add_action( 'wp_ajax_get_products', 'ajax_get_products' );
add_action( 'wp_ajax_nopriv_get_products', 'ajax_get_products' );

function ajax_get_products() {
  $args = array(
    'post_type' => 'product',
    'posts_per_page' => 32,
    'offset' => $_GET['offset']
  );
  $loop = new WP_Query( $args );
  if ( $loop->have_posts() ) {
    while ( $loop->have_posts() ) : $loop->the_post();
    wc_get_template_part( 'content', 'product' );
  endwhile;
}
    die();
}

add_action( 'wp_ajax_get_products_json', 'ajax_get_products_json' );
add_action( 'wp_ajax_nopriv_get_products_json', 'ajax_get_products_json' );

function ajax_get_products_json() {
  $products = array();
  $args = array(
    'post_type' => 'product',
    'posts_per_page' => 32,
    'offset' => $_GET['offset']
  );
  $loop = new WP_Query( $args );
  if ( $loop->have_posts() ) {
    while ( $loop->have_posts() ) : $loop->the_post();
    $product = array();
    $product['id'] = get_the_ID();
    $product['name'] = get_the_title();
    $product['image'] = htmlspecialchars(woocommerce_get_product_thumbnail());

    $brand_terms = get_the_terms($product['ID'], "brand");
    $brands = array();
    foreach($brand_terms as $term){
    	array_push($brands, $term->slug);
    }

    $style_terms = get_the_terms($product['ID'], "style");
    $styles = array();
    foreach($style_terms as $term){
    	array_push($styles, $term->slug);
    }

    $cat_terms = get_the_terms($product['ID'], "product_cat");
    $cats = array();
    foreach($cat_terms as $term){
    	array_push($cats, $term->slug);
    }

    $type_terms = get_the_terms($product['ID'], "domo_product_type");
    $types = array();
    foreach($type_terms as $term){
    	array_push($types, $term->slug);
    }

    $upholstery_terms = get_the_terms($product['ID'], "upholstery");
    $upholstery = array();
    foreach($upholstery_terms as $term){
    	array_push($upholstery, $term->slug);
    }

    $product['brand'] = implode(" ", $brands);
    $product['style'] = implode(" ", $styles);
    $product['product_cat'] = implode(" ", $cats);
    $product['domo_product_type'] = implode(" ", $types);
    $product['upholstery'] = implode(" ", $upholstery);

    array_push($products, $product);
  endwhile;
}
    print json_encode($products);
    die();
}

function get_products_as_json($amount, $offset, $brand, $style, $cat, $type, $upholstery){
  $tax_query = array();
  if ($brand != null || $style != null || $cat != null || $type != null || $upholstery != null){
    $tax_query['relation'] = "AND";
    if ($brand != null){
      $filter = array('taxonomy' => 'brand', 'field' => 'slug', 'terms' => array($brand));
      array_push($tax_query, $filter);
    }

    if ($style != null){
      $filter = array('taxonomy' => 'style', 'field' => 'slug', 'terms' => array($style));
      array_push($tax_query, $filter);
    }

    if ($cat != null){
      $filter = array('taxonomy' => 'product_cat', 'field' => 'slug', 'terms' => array($cat));
      array_push($tax_query, $filter);
    }

    if ($type != null){
      $filter = array('taxonomy' => 'domo_product_type', 'field' => 'slug', 'terms' => array($type));
      array_push($tax_query, $filter);
    }

    if ($upholstery != null){
      $filter = array('taxonomy' => 'upholstery', 'field' => 'slug', 'terms' => array($upholstery));
      array_push($tax_query, $filter);
    }
  }
  print_r($tax_query);
}

remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);

add_action( 'pre_get_posts', 'custom_reverse_post_order' );
function custom_reverse_post_order( $query ) {
	if ( is_admin() )
		return;
    if ( is_post_type_archive('store') ) {
		$query->set( 'orderby', 'date' );
		$query->set( 'order', 'ASC' );
	}
}

function domo_get_user_roles( $user = null ) {
	$user = $user ? new WP_User( $user ) : wp_get_current_user();
	return $user->roles ? $user->roles : false;
}

function domo_get_membership_level (){
  if (domo_roles_contain("gold-membership") || domo_roles_contain("administrator") || domo_roles_contain("editor") || domo_roles_contain("shop_manager")){
    return "gold";
  }

  if (domo_roles_contain("silver-membership")){
    return "silver";
  }

  if (domo_roles_contain("bronze-membership")){
    return "bronze";
  }

  return "none";
}

function domo_roles_contain($role){
  $roles = domo_get_user_roles();
  foreach((array)$roles as $r){
    if ($r == $role){
      return true;
    }
  }
  return false;
}

function domo_login( $atts ){
  return '<form action="' . get_permalink( get_option('woocommerce_myaccount_page_id') ) . '" method="post">
    <fieldset>
      <input type="email" id="username" name="username" placeholder="Email Address">
    </fieldset>
    <fieldset>
      <input type="password" id="password" name="password" placeholder="Password">
    </fieldset>
    <fieldset>
      <input type="submit" name="login" id="wp_submit" class="view_more_btn" value="Login">
    </fieldset>
    <fieldset>
      <small><a href=" ' . site_url() . "/my-account/lost-password" . '">Forgot Password?</a></small>
    </fieldset>' . wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ) .
    '<input type="hidden" name="redirect_to" value="' . ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] . '">
  </form>';
}
add_shortcode( 'domo_login', 'domo_login' );

/*add to cart ajax*/
add_filter( 'woocommerce_add_to_cart_fragments', 'woocommerce_header_add_to_cart_fragment' );
function woocommerce_header_add_to_cart_fragment( $fragments ) {
	ob_start();
?>
<a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php echo sprintf (_n( '%d', '%d', WC()->cart->get_cart_contents_count() ), WC()->cart->get_cart_contents_count() ); ?> </a>
<?php
$fragments['a.cart-contents'] = ob_get_clean();
	return $fragments;
}

function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/img/domo-logo-master.svg);
            padding-bottom: 30px;
            width: 100%;
            background-size: contain;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


function add_admin_bar() {

global $wp_admin_bar;

$wp_admin_bar->add_menu( array(
 'id' => 'site_tools',
 'title' => __( 'Site Tools'),
 'href' => __(''),
 ) );

 $wp_admin_bar->add_menu( array(
 'parent' => 'site_tools',
 'id' => 'clear_cache',
 'title' => "Refresh product cache",
 'href' => get_stylesheet_directory_uri() . "/woocommerce/cache-all-products.php",
 ));

}

add_action('admin_bar_menu', 'add_admin_bar',100);



	function update_wishlist_count() {
		global $yith_wcwl;
		$data = array(
	        'wishlist_count_products' => class_exists('YITH_WCWL') ? yith_wcwl_count_products() : 0,
	    );
		wp_send_json($data);
	}
	add_action( 'wp_ajax_update_wishlist_count', 'update_wishlist_count' );
	add_action( 'wp_ajax_nopriv_update_wishlist_count', 'update_wishlist_count' );

add_filter( 'get_avatar' , 'my_custom_avatar' , 1 , 5 );

function my_custom_avatar() {
  return "<img src='" . get_stylesheet_directory_uri() . "/img/small-comment-icon.svg' width='50px' />";
}

function wpb_move_comment_field_to_bottom( $fields ) {
$comment_field = $fields['comment'];
unset( $fields['comment'] );
$fields['comment'] = $comment_field;
return $fields;
}

add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );

add_action( 'init', 'add_rewrite_rules' );
function add_rewrite_rules() {
    add_rewrite_rule(
        "shop/all",
        "index.php?post_type=product&nocache=true",
        "top");
}

//currency symbol
function currency_symbol_sign( $currency_symbol, $currency ) {
    switch( $currency ) {
        case 'USD':
            $currency_symbol = 'USD $';
            break;
        case 'AUD':
            $currency_symbol = 'AUD $';
            break;
    }
    return $currency_symbol;
}
add_filter('woocommerce_currency_symbol', 'currency_symbol_sign', 30, 2);

//showing brand on admin filter
function add_taxonomy_filters() {
	global $typenow;

	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
	$taxonomies = array('brand');

	// must set this to the post type you want the filter(s) displayed on
	if( $typenow == 'product' ){

		foreach ($taxonomies as $tax_slug) {
			$tax_obj = get_taxonomy($tax_slug);
			$tax_name = $tax_obj->labels->name;
			$terms = get_terms($tax_slug);
			if(count($terms) > 0) {
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>Show All $tax_name</option>";
				foreach ($terms as $term) {
					echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
				}
				echo "</select>";
			}
		}
	}
}
add_action( 'restrict_manage_posts', 'add_taxonomy_filters' );

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     $fields['billing']['billing_pickup_store'] = array(
       'type' => 'select',
        'label'     => __('Preferred store for pickup', 'woocommerce'),
    'placeholder'   => _x('Phone', 'placeholder', 'woocommerce'),
    'required'  => true,
    'class'     => array('form-row-wide'),
    'clear'     => true
     );

     $fields['billing']['billing_pickup_store']['options'] = array();

     // Get a list of stores
     $stores = get_posts(
      array(
        'posts_per_page' => -1,
        'post_type' => 'store',
       )
     );

     foreach($stores as $store){
       $fields['billing']['billing_pickup_store']['options'][$store->post_name] = $store->post_title;
     }

     return $fields;
}

/**
 * Display field value on the order edit page
 */

add_action( 'woocommerce_admin_order_data_after_shipping_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

function my_custom_checkout_field_display_admin_order_meta($order){
    /* Fetch the store from the slug */
    $storeSlug = get_post_meta($order->id, '_billing_pickup_store', true);
    $store = get_store_from_slug($storeSlug);
    print_r($store);
    echo '<p><strong>'.__('Preferred store for pickup').':</strong> ' . $store->post_title . '</p>';
}

function get_store_from_slug($slug) {
  $args = array(
    'name'        => $slug,
    'post_type'   => 'store',
    'post_status' => 'publish',
    'numberposts' => 1
  );

  $stores = get_posts($args);
  if( $stores ) :
    return $stores[0];
  endif;

  return null;
}

function order_email_recipient_filter_function($recipient, $order) {
    /* Get the store that was selected for pickup */
    $storeSlug = get_post_meta($order->id, '_billing_pickup_store', true);
    $store = get_store_from_slug($storeSlug);

    if ($store != null) {
      $recipient = $recipient . ', ' . get_field("email", $store->ID);
    }
    return $recipient;
}

/* Add the store selected for pickup to the email */
add_filter( 'woocommerce_email_recipient_new_order', 'order_email_recipient_filter_function', 10, 2);

/* Creates a function for sending an email verification to an user */
/*
WP_User Object ( [data] => stdClass Object ( [ID] => 500 [user_login] => test101@test.com [user_pass] => $P$BEs9gfwN3FKs9rgxrSJCwp81d2/Fzx1 [user_nicename] => test101test-com [user_email] => test101@test.com [user_url] => [user_registered] => 2017-03-02 23:06:41 [user_activation_key] => [user_status] => 0 [display_name] => test101@test.com [user_level] => 0 ) [ID] => 500 [caps] => Array ( [customer] => 1 ) [cap_key] => wp_capabilities [roles] => Array ( [0] => customer ) [allcaps] => Array ( [read] => 1 [customer] => 1 ) [filter] => )
*/
function send_email_verification($userID) {
  $template = file_get_contents(get_stylesheet_directory() . "/email-templates/email-verify.html");
  $data = get_userdata($userID);
  $firstName = $data->first_name;
  $email = $data->user_email;

  $template = str_replace("{{firstName}}", $firstName, $template);
  $template = str_replace("{{userID}}", $userID, $template);
  $template = str_replace("{{email}}", $email, $template);

  /* Generate the code for the user */
  $code = generateRandomString();

  $template = str_replace("{{verifyCode}}", $code, $template);
  update_user_meta($userID, "email_verified", "false");
  update_user_meta($userID, "verify_code", $code);
  wp_mail($email, "Email verification", $template, array('Content-Type: text/html; charset=UTF-8'), array());
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function send_welcome_email($firstName, $email, $user_type, $data){

  if ($user_type == "retail"){
    $template = file_get_contents(get_stylesheet_directory() . "/email-templates/welcome-retail.html");
    // Replace the placeholders
    $template = str_replace("{{firstName}}", $firstName, $template);
    $template = str_replace("{{content}}", get_field("retail_welcome_email_content", 17), $template);
    // Send the email
    wp_mail($email, "Welcome to DOMO", $template, array('Content-Type: text/html; charset=UTF-8'), array());
    // Also send a mail to the admin
    $newUserTemplate = file_get_contents(get_stylesheet_directory() . "/email-templates/admin-new-user.html");
    $newUserTemplate = str_replace("{{firstName}}", $data['firstName'], $newUserTemplate);
    $newUserTemplate = str_replace("{{lastName}}", $data['lastName'], $newUserTemplate);
    $newUserTemplate = str_replace("{{userType}}", $data['userType'], $newUserTemplate);
    $newUserTemplate = str_replace("{{email}}", $data['email'], $newUserTemplate);
    $newUserTemplate = str_replace("{{postcode}}", $data['postcode'], $newUserTemplate);
    $newUserTemplate = str_replace("{{phone}}", $data['phone'], $newUserTemplate);
    $newUserTemplate = str_replace("{{company}}", $data['company'], $newUserTemplate);
    wp_mail(get_field(143, "new_user_contact_email"), "New member signup - domo.com.au", $newUserTemplate, array('Content-Type: text/html; charset=UTF-8'), array());
  }

  if ($user_type == "trade"){
    $template = file_get_contents(get_stylesheet_directory() . "/email-templates/welcome-trade.html");
    $template = str_replace("{{firstName}}", $firstName, $template);
    $template = str_replace("{{content}}", get_field("trade_welcome_email_content", 17), $template);
    $store = get_store_from_slug($data['shipping_showroom']);
    $storeEmail = get_field("email", $store->ID);

    $template = str_replace("{{store_email}}", $storeEmail, $template);

    //Get all the files to attach
    $files = array();
    $file_array = get_field("trade_welcome_email_attachments", 17);
    foreach($file_array as $file){
      array_push($files, get_attached_file($file['file']['ID']));
    }
    // Send the email
    wp_mail($email, "Welcome to DOMO", $template, array('Content-Type: text/html; charset=UTF-8'), $files);
    // Also send a mail to the admin
    $newUserTemplate = file_get_contents(get_stylesheet_directory() . "/email-templates/admin-new-user.html");
    $newUserTemplate = str_replace("{{firstName}}", $data['firstName'], $newUserTemplate);
    $newUserTemplate = str_replace("{{lastName}}", $data['lastName'], $newUserTemplate);
    $newUserTemplate = str_replace("{{userType}}", $data['userType'], $newUserTemplate);
    $newUserTemplate = str_replace("{{email}}", $data['email'], $newUserTemplate);
    $newUserTemplate = str_replace("{{postcode}}", $data['postcode'], $newUserTemplate);
    $newUserTemplate = str_replace("{{phone}}", $data['phone'], $newUserTemplate);
    $newUserTemplate = str_replace("{{company}}", $data['company'], $newUserTemplate);
    $newUserTemplate = str_replace("{{store_email}}", $storeEmail, $newUserTemplate);
    wp_mail(get_field(143, "new_user_contact_email"), "New contract member signup - domo.com.au", $newUserTemplate, array('Content-Type: text/html; charset=UTF-8'), array());
    wp_mail($storeEmail, "New contract member signup - domo.com.au", $newUserTemplate, array('Content-Type: text/html; charset=UTF-8'), array());
  }

}

//defining the filter that will be used so we can select posts by 'author'
function add_offer_filter_to_posts_administration(){

    //execute only on the 'post' content type
    global $post_type;
    if($post_type == 'product'){
      ?>
      <select name="product-offer-filter-value">
        <option value="*">All offers</option>
        <option value="sale" <?php if ($_GET['product-offer-filter-value'] == "sale"){ Print "selected"; } ?>>Sale Items</option>
        <option value="click-collect" <?php if ($_GET['product-offer-filter-value'] == "click-collect"){ Print "selected"; } ?>>Click & Collect Items</option>
        <option value="clearance" <?php if ($_GET['product-offer-filter-value'] == "clearance"){ Print "selected"; } ?>>Clearance Items</option>
      </select>
      <?php
    }

}
add_action('restrict_manage_posts','add_offer_filter_to_posts_administration');

function apply_offer_filter( $query ) {

global $pagenow;

// Ensure it is an edit.php admin page, the filter exists and has a value, and that it's the products page
if ( $query->is_admin && $pagenow == 'edit.php' && isset( $_GET['product-offer-filter-value'] ) && $_GET['product-offer-filter-value'] != '' && $_GET['post_type'] == 'product' ) {

  $filter = $_GET['product-offer-filter-value'];

  if ($filter == 'sale') {
    $meta_key_query = array(
      array(
        'key'     => 'product_on_sale',
        'value'   => 1,
      )
    );
    $query->set( 'meta_query', $meta_key_query );
  }

  if ($filter == 'clearance') {
    $meta_key_query = array(
      array(
        'key'     => 'product_on_clearance',
        'value'   => 1,
      )
    );
    $query->set( 'meta_query', $meta_key_query );
  }

  if ($filter == 'click-collect') {
    $meta_key_query = array(
      array(
        'key'     => 'available_for_online_purchase',
        'value'   => 1,
      )
    );
    $query->set( 'meta_query', $meta_key_query );
  }

}

}

add_action( 'pre_get_posts', 'apply_offer_filter' );

function xyz_filter_wp_mail_from($email){
  return "noreply@domo.com.au";
}

add_filter("wp_mail_from", "xyz_filter_wp_mail_from");



add_action( 'after_setup_theme', 'yourtheme_setup' );

function yourtheme_setup() {
//add_theme_support( 'wc-product-gallery-zoom' );
add_theme_support( 'wc-product-gallery-lightbox' );
add_theme_support( 'wc-product-gallery-slider' );
}



/*getting filter section on product*/

//defining the filter that will be used so we can select posts by 'author'
/*function domo_showroom_filter(){

    //execute only on the 'post' content type
    global $post_type;

    if($post_type == 'product'){
      ?>
      <select name="showroom-filter">
        <option value="*">Select Showrooms</option>
        <option value="sandringham" <?php if ($_GET['showroom-filter'] == "sandringham"){ Print "selected"; } ?>>Sandringham</option>
        <option value="hawthorn" <?php if ($_GET['showroom-filter'] == "hawthorn"){ Print "selected"; } ?>>Hawthorn</option>
      </select>
      <?php
    }

}
add_action('restrict_manage_posts','domo_showroom_filter');

function apply_showroom_filter( $query ) {

global $search_Store;
// Ensure it is an edit.php admin page, the filter exists and has a value, and that it's the products page
if ( $query->is_admin && $search_Store == 'edit.php' && isset( $_GET['showroom-filter'] ) && $_GET['showroom-filter'] != '' && $_GET['post_type'] == 'product' ) {
  $filter = $_GET['showroom-filter'];

  if ($filter == 'sandringham') {
    $meta_key_query = array(
      array(
        'key'     => 'available_domo_stores',
        'value'   => true,
      )
    );
    $query->set( 'meta_query', $meta_key_query );
  }

  if ($filter == 'hawthorn') {
    $meta_key_query = array(
      array(
        'key'     => 'available_domo_stores',
        'value'   => true,
      )
    );
    $query->set( 'meta_query', $meta_key_query );
  }



}
}
add_action( 'pre_get_posts', 'apply_showroom_filter' );*/

add_filter( 'get_terms_args', 'wpse_53094_sort_get_terms_args', 10, 2 );
function wpse_53094_sort_get_terms_args( $args, $taxonomies )
{
    global $pagenow;
    if( !is_admin() || ('post.php' != $pagenow && 'post-new.php' != $pagenow) )
        return $args;

    $args['orderby'] = 'slug';
    $args['order'] = 'DESC';

    return $args;
}
