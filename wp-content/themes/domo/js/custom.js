$(function() {

  $('button.navbar-toggle').click(function(e){
    $('html, body').toggleClass('out');
    $('nav.custom_nav').toggleClass('out');
    if ($('body').hasClass('out')) {
      $(this).focus();
    } else {
      $(this).blur();
    }
  });

  $('body').on({
    'click touchstart': function (e) {
      if ($('body').hasClass('out') && !$(e.target).closest('.navbar-collapse, button.navbar-toggle').length) {
        e.preventDefault();
        $('button.navbar-toggle').trigger('click');
        $('button.navbar-toggle').blur();
      }
    },
    keyup: function (e) {
      if (e.keyCode == 27 && $('body').hasClass('out')) {
        $('button.navbar-toggle').trigger('click');
      }
    }
  });

});


$(document).ready(function() {
  "use strict";
  $(".banner-slide").owlCarousel({
    navigation : true, // Show next and prev buttons
    slideSpeed : 300,
    paginationSpeed : 400,
    singleItem:false,
    items:1,
    autoplay: true,
    rewindNav: true,
    autoHeight : true,
    loop: true,
    autoplaySpeed: 1200,
  });

  $('.online-product-slider').owlCarousel({
    loop:false,
    margin:20,
    nav:true,
    navText: ['',''],
    responsive:{
      0:{
        items:1
      },
      480:{
        items:2
      },
      768:{
        items:3
      },
      992:{ items: 4}
    }
  });



  $('.extra_gallery_slider.owl-carousel').owlCarousel({
    loop:false,
    margin:20,
    nav:true,
    navText: ['',''],
    responsive:{
      0:{
        items:1
      },
      550:{
        items:2
      },
      767:{
        items:2
      },
      1000:{
        items:3
      }
    }
  });

  $('.thumbnails').owlCarousel({
    loop: false,
    margin: 10,
    nav: true,
    autoplay: true,
    autoplayTimeout: 5000,
    autoplayHoverPause: false,
    autoplaySpeed: 1200,
    navText: ['', ''],
    responsive: {
      0: {
        items: 3
      },
      520: {
        items: 3
      },
      768: {
        items: 3
      },
      992: {
        items: 4
      }
    }
  });


});


function valueChanged()
{
  if($('#checkedbox').is(":checked")){
    $(".newsletter-form.second").show();
    $(".newsletter-form.first").hide();
    $(".member-benefits.second").show();
    $(".member-benefits.first").hide();
  } else {
    $(".newsletter-form.second").hide();
    $(".newsletter-form.first").show();
    $(".member-benefits.second").hide();
    $(".member-benefits.first").show();
  }

}

jQuery(function($) {
  "use strict";
  if($(window).width()>972){

    $('.navbar .dropdown').hover(function() {
      $(this).find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
    }, function() {
      $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp();
    });
    // $('li.menu-item-has-children a').click(function(e){
    //   //e.preventDefault();
    //   if ($(this).parent().find(".dropdown-menu").hasClass("open") == false){
    //     $(".dropdown-menu.open").removeClass("open").slideUp();
    //     $(this).parent().find(".dropdown-menu").first().stop(true, true).addClass("open").slideDown();
    //   } else {
    //     $(".dropdown-menu.open").removeClass("open").slideUp();
    //   }
    //
    // });

  }
});

function showDropdown(){
  $('header').find('.dropdown-menu').first().stop(true, true).delay(250).slideDown();
}

/* search toggle for mobile*/
$(document).ready(function() {
  "use strict";
  $('.search-icon-mobile').click(function(e){
    e.preventDefault();
    //get collapse content selector
    var collapse_content_selector = $(this).attr('href');
    //make the collapse content to be shown or hide
    var toggle_switch = $(this);
    $(collapse_content_selector).toggle(function(){
      if($(this).css('display')==='none'){
        //change the button label to be 'search'
        toggle_switch.html('<i class="fa fa-search"></i>');
      }else{
        //change the button label to be 'cross'
        toggle_switch.html('<i class="fa fa-times "></i>');
      }
    });
  });
});

$(document).ready(function(){
  "use strict";
  if($(window).width()<470){
    $(".footer-dropdown-list .menu-item-has-children>h4").click(function(){
      $(this).toggleClass("showmenu");
      $(".footer-dropdown-list .menu-item-has-children>h4").not(this).parent().find("ul").slideUp();
      $(this).parent().find("ul").slideToggle("slow");
    });
  }
});


$(document).ready(function(){
  "use strict";
  $(".login_signup a").click(function(){
    $(".sign-in-box").slideToggle();
  });
  $(".closebox a").click(function(e){
    e.preventDefault();
    $(".sign-in-box").slideUp('slow');
  });
});

$(".qty-more-btn").click(function(){
  var value = $(this).parent().find("input[type=number]").val();
  value++;
  $(this).parent().find("input[type=number]").val(value);
});

$(".qty-less-btn").click(function(){
  var value = $(this).parent().find("input[type=number]").val();
  value--;
  if (value < 1){
    value = 1;
  }
  $(this).parent().find("input[type=number]").val(value);
});

// equal height
function equalHeight(group) {
  var tallest = 0;
  group.each(function() {
    var thisHeight = $(this).height();
    if(thisHeight > tallest) {
      tallest = thisHeight;
    }
  });
  group.height(tallest);
}

$(document).ready(function(){
  "use strict";
  equalHeight($(".itemslider"));
  equalHeight($(".news_content"));
  equalHeight($(".other_blog_post article"));
  $(".google-map").each(function(){
    var element = $(this);
    var lat = parseFloat(element.attr("data-lat"));
    var lng = parseFloat(element.attr("data-lng"));
    var myLatLng = {lat: lat, lng: lng};
    console.log(element);
    var map = new google.maps.Map(element[0], {
      zoom: 14,
      center: myLatLng
    });
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: 'DOMO Store Location',
      icon: 'https://www.domo.com.au/wp-content/themes/domo/img/domo-map-pin.png'
    });
  });

  $(".stockist_map").each(function(){
    var element = $(this);
    var lat = parseFloat(element.attr("data-lat"));
    var lng = parseFloat(element.attr("data-lng"));
    var myLatLng = {lat: lat, lng: lng};
    console.log(element);
    var map = new google.maps.Map(element[0], {
      zoom: 14,
      center: myLatLng
    });
    var marker = new google.maps.Marker({
      position: myLatLng,
      map: map,
      title: 'DOMO Store Location',
      icon: 'https://www.domo.com.au/wp-content/themes/domo/img/marker_teal.png'
    });
  });


});


$(document).ready(function(){
  var oldImg;
  $(".menu-item-object-brand_profile a[data-hover-image]").hover(function(){
    oldImg = $(".brand-hover-menu-img .menu-image-holder").css("background-image");
    //Swap out the image
    $(".brand-hover-menu-img .menu-image-holder").css("background-image", "url(\"" + $(this).attr("data-hover-image") + "\")");
  }, function(){
    $(".brand-hover-menu-img .menu-image-holder").css("background-image", oldImg);
  });

  $(".quick-connect-selector").change(function(){
    $(".quick-connect-results div").hide();
    $(".quick-connect-results .results-state-" + $(this).val()).fadeIn();
  });
});

$(document).ready(function(){

  $(".fakefilepicker").parent().find("input").on('change', function(){
    var fullPath = $(".fakefilepicker").parent().find("input").val();
    if (fullPath) {
      var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
      var filename = fullPath.substring(startIndex);
      if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
        filename = filename.substring(1);
      }
      $(".fakefilepicker").html("<i class='fa fa-times' aria-hidden='true'></i> " + filename);
    }
  });
});

$(document).ready(function(){
  try {
    if (createProductFilters != null){
      createProductFilters();
    }

    if (createProducts != null){
      createProducts();
    }
  } catch (e){}

});


// to hide top nav when scroll down and show when scroll up
var senseSpeed = 5;
var previousScroll = 0;
jQuery(window).scroll(function(){
  "use strict";
  var scroller = jQuery(this).scrollTop();
  if (scroller-senseSpeed > previousScroll){
    jQuery(".latest-arrivals-banner-text, .domo-promotion-banner h1").filter(':not(:animated)').fadeOut(1400);
  } else if (scroller+senseSpeed < previousScroll) {
    jQuery(".latest-arrivals-banner-text, .domo-promotion-banner h1").filter(':not(:animated)').fadeIn(1400);
  }
  previousScroll = scroller;
});

$(document).ready(function() {
    (function($) {
        var MutationObserver = window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver;

        $.fn.attrchange = function(callback) {
            if (MutationObserver) {
                var options = {
                    subtree: false,
                    attributes: true
                };

                var observer = new MutationObserver(function(mutations) {
                    mutations.forEach(function(e) {
                        callback.call(e.target, e.attributeName);
                    });
                });

                return this.each(function() {
                    observer.observe(this, options);
                });

            }
        }
    })(jQuery);

    $('.yith-wcwl-wishlistaddedbrowse').attrchange(function(attrName) {
        if ( attrName == 'class' ){
          /* Request wishlist update */
          $.ajax({
            data : {
            action: 'update_wishlist_count'
            },
            success : function (data) {
              console.log("Updated");
              console.log(data);
              var newCount = data["wishlist_count_products"];
              $(".ajax-wishlist-count").text(newCount);
            },
            url: yith_wcwl_l10n.ajax_url
          });
        }
    });
});
