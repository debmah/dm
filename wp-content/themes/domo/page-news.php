<?php get_header();
/* Template Name: News */
?>
<div class="content ptb-1em">
	<div class="container">
    	<div class="row clearfix">
        	<div class="single-left col-sm-8 col-md-9 mmb-2em">
            	<div class="news-archives">

                    	<?php
							$page = (get_query_var('paged')) ? get_query_var('paged') : 1;
							$temp = $wp_query;
							$wp_query = null;
							$wp_query = new WP_Query();
							$wp_query -> query('post_type=post&showposts=6'.'&paged='.$paged);
							while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
                            <?php $thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
							$thumb_url = $thumb_url_array[0]; ?>


                            <article class="clearfix">
                                    <a href="<?php the_permalink(); ?>"><figure style="background-image: url('<?php echo $thumb_url; ?>');"></figure></a>
                                    <div class="blog-minicontent">
                                        <header>
                                            <h4><a href="<?php the_permalink(); ?>"><?php $post_title = get_the_title(); echo substr($post_title, 0, 60); ?> </a></h4>
                                            <div class="posted_date_month"><span class="current_post_date"><?php $date_store = date("d", strtotime($post->post_date)); echo ltrim($date_store, 0); ?></span><span class="current_post_month text-uppercase"><?php echo date("M", strtotime($post->post_date)); ?></span></div>
                                        </header>
                                        <div class="post-meta"><small>POSTED BY DOMO</small></div>
                                        <div class="related_post_cat pb-1em">
                                            <?php  $category = get_the_category();
                                            foreach ($category as $outpuvalue){ ?>
																							<span><a href="<?php echo get_category_link( $outpuvalue->term_id ); ?>"><?php echo $outpuvalue->cat_name; ?></a></span>
                                                
                                            <?php }
                                            ?>
                                        </div>
                                        <p><?php $excerpt = get_the_content(); if (strlen($excerpt) > 255) { $excerpt = substr($excerpt, 0, 255) . "..."; echo $excerpt; } ?></p>
                                        <a href="<?php the_permalink(); ?>" class="read_more_btn_outline">read</a>
                                    </div>
                            </article>

                            <?php endwhile; ?>


                        <!-- Pagination Nav -->
                        <div class="pagination-nav">
                             <?php the_posts_pagination( $args ); ?>
                            <?php $wp_query = null; $wp_query = $temp; ?> <!-- this is to clear wp_query -->
                        </div>
                        <!-- End Pagination Nav -->
                </div>




            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>

<?php //get_sidebar(); ?>
<?php get_footer(); ?>
