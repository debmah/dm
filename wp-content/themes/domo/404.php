<?php
get_header(); ?>
<div class="content ptb-2em">
  <div class="container">
    <div class="inner-full-width text-center page404">
      <img src='<?php Print get_stylesheet_directory_uri(); ?>/img/domo-error-2.svg' width='400px' />
      <!-- <h1>404</h1> -->
      <h2>PAGE NOT FOUND</h2>
      <p>The page you are looking for doesn't seem to be here anymore?</p>
      <p>Click back or click the button below to return to the home page.</p>
      <a href='<?php Print get_site_url(); ?>' class='btn'>GO TO THE HOME PAGE</a>
    </div>
  </div>
</div>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>
