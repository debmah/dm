<?php get_header(); ?>
<div class="content ptb-2em">
<div class="container">
	
    <div class="single-tip-care blog-left-column">
    	<div class="row clearfix">
        	<div class="col-sm-8 col-md-9 full_img">
            	<?php the_post_thumbnail(); ?>
            </div>
            <div class="col-sm-4 col-md-3 care_usage_sidebar hidden-xs">
            	<h4>SHARE THIS ONLINE</h4>
                <div class="social_icons pt-1em pb-2em">
                    <a href="http://www.facebook.com/share.php?u=<?php print(urlencode(get_permalink())); ?>&title=<?php print(urlencode(the_title())); ?>" target="_blank" class="fb"><i class="fa fa-facebook"></i></a>
                    <a href="http://twitter.com/intent/tweet?status=<?php print(urlencode(the_title())); ?>+<?php print(urlencode(get_permalink())); ?>" target="_blank" class="tt"><i class="fa fa-twitter"></i></a>
                    <!-- <a href="" class="ig"><i class="fa fa-instagram"></i></a> -->
                    <a href="http://pinterest.com/pin/create/bookmarklet/?media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' ); print(urlencode($url = $thumb['0']));?>
    &url=<?php print(urlencode(the_permalink())); ?>&is_video=false&description=<?php print(urlencode(get_the_title())); ?>" target="_blank" class="pt"><i class="fa fa-pinterest"></i></a>
				</div>
                <div class="other-tips">
                	<h4>Other promotions</h4>
                    <ul class="other_tips_lists">
                	<?php
                    $currentID = get_the_ID();
                    $my_query = new WP_Query( array('showposts' => '4', 'post_type' => 'promotion', 'orderby' => 'rand', 'post__not_in' => array($currentID)));
                    
                    while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                    <?php endwhile; ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clearfix full_content_promotion pt-2em">
        	<?php while(have_posts()):the_post(); ?>
                <header class="pagetitle_header"><?php echo get_the_title(); ?></header>
                <?php the_content(); ?>
                <?php if(get_field('button_link')): ?><div class="enter-promotion"><a target="_blank" href="<?php echo get_field('button_link'); ?>" class="view_more_btn"><?php echo get_field('button_text'); ?></a></div><?php endif; ?>
            <?php endwhile; ?>
        </div>
        
        <!-- this is care usage sidebar, only visible on mobile -->
        	<div class="col-sm-4 col-md-3 care_usage_sidebar only_mobile_visible visible-xs">
            	<h4>SHARE THIS ONLINE</h4>
                <div class="social_icons pt-1em pb-2em">
                    <a href="http://www.facebook.com/share.php?u=<?php print(urlencode(get_permalink())); ?>&title=<?php print(urlencode(the_title())); ?>" target="_blank" class="fb"><i class="fa fa-facebook"></i></a>
                    <a href="http://twitter.com/intent/tweet?status=<?php print(urlencode(the_title())); ?>+<?php print(urlencode(get_permalink())); ?>" target="_blank" class="tt"><i class="fa fa-twitter"></i></a>
                    <!-- <a href="" class="ig"><i class="fa fa-instagram"></i></a> -->
                    <a href="http://pinterest.com/pin/create/bookmarklet/?media=<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'thumbnail' ); print(urlencode($url = $thumb['0']));?>
    &url=<?php print(urlencode(the_permalink())); ?>&is_video=false&description=<?php print(urlencode(get_the_title())); ?>" target="_blank" class="pt"><i class="fa fa-pinterest"></i></a>
				</div>
                <div class="other-tips">
                	<h4>Other promotions</h4>
                    <ul class="other_tips_lists">
                	<?php
                    $currentID = get_the_ID();
                    $my_query = new WP_Query( array('showposts' => '4', 'post_type' => 'promotion', 'orderby' => 'rand', 'post__not_in' => array($currentID)));
                    
                    while ( $my_query->have_posts() ) : $my_query->the_post(); ?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                    <?php endwhile; ?>
                    </ul>
                </div>
            </div>
    </div>
</div>
</div>
<?php get_footer(); ?>
