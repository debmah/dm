<div class="sidebar col-sm-4 col-md-3">
  <?php /*?><div class="popular_blog sidebar_widget">
    <h4>Popular Blogs</h4>
    <ul>
      <?php query_posts('meta_key=post_views_count&orderby=meta_value_num&order=DESC&posts_per_page=1');
	if (have_posts()) : while (have_posts()) : the_post(); ?>
      <li> <a href="<?php the_permalink(); ?>">
        <?php the_post_thumbnail('thumbnail'); ?>
        <span>
        <?php the_title(); ?>
        </span>
        <?php the_excerpt(); ?>
        </a> </li>
      <?php endwhile; endif; wp_reset_query(); ?>
    </ul>
  </div><?php */?>
  <div class="post_categories sidebar_widget">
    <h4>Categories</h4>
    <ul>
      <?php wp_list_categories( array( 'title_li' => '' ) ); ?>
    </ul>
  </div>
  <div class='post_tags sidebar_widget'>
    <h4>Tags</h4>
    <?php
    if (is_single()){
      echo get_the_tag_list('','','');
    } else {
      $tags = get_tags();
      foreach($tags as $tag){
        print_r("<a href='" . get_tag_link($tag->term_id) . "'>" . strtolower($tag->name) . "</a>");
      }
    }
    ?>
  </div>
  <div class="instagram_feed sidebar_widget">
    <h4 class="clearfix instagram-feed-txt">OUR INSTAGRAM FEED</h4>
    <div class="instagram-feed-list">
    	<?php echo do_shortcode('[instagram-feed]'); ?>
    </div>
  </div>

  <div class="domo-newsletter-sidebar sidebar_widget">
        <h3>DOMO NEWS <br>& OFFERS</h3>
        <h5>Get the best of DOMO delivered to you monthly, plus discounts and offers.</h5>
        <a href="<?php Print get_site_url(); ?>/sign-up" class="view_more_btn">SIGN UP</a>
  </div>

  <div class="domo-care-usage sidebar_widget">
  	<h3>DOMO <span>CARE & USAGE</span></h3>
    <p>Learn useful tips on how to care for your furniture, the upholstery and clean those valuable items.</p>
    <figure class="pb-1em"><img src="<?php bloginfo('template_url'); ?>/img/care-and-usage-sidebar-image.jpg" alt="care and usage"></figure>
    <div class="text-center"><a href="<?php Print get_site_url(); ?>/care-and-usage" class="view_more_btn">VIEW CARE & USAGE</a></div>
  </div>

  <div class="news_archive sidebar_widget">
  	<h4>News Archive</h4>
    <ul class="post_categories">
		<?php wp_get_archives( $args ); ?>
    </ul>
  </div>

</div>
