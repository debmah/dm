<?php
/*
Plugin Name: 	New User Report
Plugin URI:		http://foxandlee.com.au
Description: 	Generates a weekly CSV container new user details
Version: 		1.0.0
Author: 		Matt Hayward
Author URI: 	http://foxandlee.com.au
*/

/**
 * Register a custom menu page.
 */
function new_user_report_register_my_custom_menu_page() {
    add_menu_page(
        __( 'New User Report', 'new_user_report' ),
        'New User Report',
        'edit_posts', //make manage_options to visible admin only on backend
        'new-user-report/views/admin.php',
        '',
        'dashicons-media-document',
        6
    );
}
add_action( 'admin_menu', 'new_user_report_register_my_custom_menu_page' );

function new_user_report_register_plugin_settings() {
  //register our settings
  register_setting( 'new-user-report', 'report_to' );
  register_setting( 'new-user-report', 'report_days' );
}

add_action( 'admin_init', 'new_user_report_register_plugin_settings' );

function new_user_report_get_option( $option_name='' ){
      //$def = array("report_to" => get_bloginfo("admin_email"), "report_days" => 7);
      $def = array("report_to" => get_bloginfo("email"), "report_days" => 7);
      return get_option($option_name, $def[$option_name]);
}

function new_user_report_run_report() {
    generate_report();
    wp_redirect( $_SERVER['HTTP_REFERER'] );
    exit();
}

function generate_report() {
  $args = [
      'date_query' => [
          [ 'after'  => new_user_report_get_option("report_days") . ' days ago', 'inclusive' => true ]
     ]
  ];
  $file = fopen(plugin_dir_path( __FILE__ ) . "/new_users.csv","w");
  //Add the header to the file
  fputcsv($file,['Date','First Name', 'Last Name', 'Username', 'Email', 'User Type', 'Postcode', 'Company', 'Selected Showroom']);
  $users = get_users($args);
  $userCount = count($users);
  foreach($users as $user){
    $date = date("j/n/Y", strtotime(get_userdata($user->ID)->user_registered));
    $firstName = get_user_meta($user->ID, 'first_name', true);
    $lastName = get_user_meta($user->ID, 'last_name', true);
    $username = $user->user_login;
    $email = $user->user_email;
    $user_type = get_user_meta($user->ID, 'user_type', true);
    $postcode = get_user_meta($user->ID, 'shipping_postcode', true);
	$company_name = get_user_meta($user->ID, 'shipping_company', true);
	$selected_showroom = get_user_meta($user->ID, 'shipping_showroom', true);
    fputcsv($file,[$date, $firstName, $lastName, $username, $email, $user_type, $postcode, $company_name, $selected_showroom]);
  }
  fclose($file);
  wp_mail(new_user_report_get_option("report_to"), "New users report - " . date("j/n/Y"), "Hi,\n\nPlease find a list of the new users attached.\n\n" . $userCount . " new user/s signed up.", $headers, array(plugin_dir_path( __FILE__ ) . '/new_users.csv'));
}

add_action( 'admin_action_new_user_report_run_report', 'new_user_report_run_report' );

function new_user_report_add_weekly( $schedules ) {
    $schedules['weekly'] = array(
        'interval' => 604800, //that's how many seconds in a week, for the unix timestamp
        'display' => __('weekly')
    );
    return $schedules;
}
add_filter('cron_schedules', 'new_user_report_add_weekly');

function run_weekly_report() {
  generate_report();
}

if( !wp_next_scheduled( 'run_weekly_report' ) ) {
    wp_schedule_event( time(), 'weekly', 'run_weekly_report' );
}

?>
