<div class="wrap">
  <style>
    .wrap input {
      font-size:14px;
      min-width:350px;
    }
  </style>
  <h1>New User Report</h1>
  <p>Below are the settings for generating the new user CSV report</p>
  <form method="post" action="options.php">
      <?php settings_fields( 'new-user-report' ); ?>
      <?php do_settings_sections( 'new-user-report' ); ?>
      <b>Recipients</b>
      <br />
      <p>The list of comma seperated email address that will receive the new user CSV report</p>
      <input type="text" name="report_to" value="<?php echo esc_attr( new_user_report_get_option('report_to') ); ?>" />
      <br /><br />
      <b>Time span</b>
      <br />
      <p>The number of days the report will show</p>
      <input type="number" name="report_days" min=0 value="<?php echo esc_attr( new_user_report_get_option('report_days') ); ?>" />
      <br />
      <?php submit_button(); ?>
  </form>
  <h2>Generate report</h2>
  <form method="post" action="<?php echo admin_url( 'admin.php' ); ?>">
    <input type="hidden" name="action" value="new_user_report_run_report">
    <?php submit_button( 'Generate' ); ?>
  </form>
</div>
